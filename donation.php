<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Latest compiled and minified CSS -->
	<link href="style.min.css" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
	      integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous"
</head>
<body>
<script src="https://use.fontawesome.com/711fd3d519.js"></script>

<header class="header-donation"> <!-- begin header -->
	<div class="container">

		<div class="row">
			<div class="col-xs-9 col-md-4">
				<div class="donation-logo-container">
					<img class="img-fluid" src="/images/donation/feed-logo.png">
				</div>
			</div>
		</div>
	</div>
</header> <!-- end header -->
<section class="hero-donation"> <!-- begin hero -->
</section> <!-- end hero -->
<section class="main-donation"> <!-- begin main -->
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-6"> <!-- main left -->
				<div class="left-col-item mt-4">
					<h2>Effectiveness and Efficiency</h2>
					<div class="left-col-inner">
						<img class="img-fluid" src="/images/donation/7x-icon.png">
						<p>Each donated dollar helps provide seven dollars worth of food, essentials, and more to hungry
							children and families.</p>
					</div>
				</div>
				<div class="left-col-item mt-4">
					<h2>Good Stewardship</h2>
					<p>93% of Feed the Children's expenditures in FY2016 went to Program Services.</p>
					<img class="img-fluid" src="/images/donation/93-percent-graph.png">
					<p>Feed the Children is 501(c)(3) non-profit organization; contributions are tax deductible as
						allowed by law.</p>
				</div>
				<div class="left-col-item mt-4">
					<h2>Transparency and Trust</h2>
					<img src="/images/donation/BBB.png">
					<img src="/images/donation/digicert-icon.png">
				</div>
			</div> <!-- end main left -->
			<div class="form-container col-sm-12 col-md-12 col-lg-6"> <!-- form right -->

				<form>
					<div class="form-group"> <!-- begin donation -->
						<h2>HELP FEED<br>
							HUNGRY CHILDREN
						</h2>
						<p><strong>This is a placeholder area for short variable copy that can be easily edited based on
								designation needs.</strong></p>
						<p>This is additional placeholder body copy that can be easily edited based on designation
							needs. A paragraph of added text would go here, in addition to the headline and subtext.</p>
						<div class="lock">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</div>
						<fieldset class="form-group">

							<h2>Donation Amount</h2>
							<div class="donation-amount">

								<input type="text" class="form-control" id="donation-25" placeholder="$25.00" readonly>

								<input type="text" class="form-control" id="donation-50" placeholder="$50.00" readonly>

								<input type="text" class="form-control" id="donation-100" placeholder="$100.00"
								       readonly>

								<input type="text" class="form-control" id="donation-150" placeholder="$250.00"
								       readonly>

							</div>

							<input type="text" class="form-control" id="donation-amount" placeholder="Other Amount">

							<p><span>$10 minimum</span></p>


							<h2>Choose Your Donation</h2>


							<div class="radio-row">
								<div class="form-check">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="optionsRadios"
										       id="optionsRadios1"
										       value="option1" checked>
										One Time Donation
									</label>
								</div>
								<div class="form-check">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="optionsRadios"
										       id="optionsRadios2"
										       value="option2">
										Monthly Donation
									</label>
								</div>
							</div>
							<div class="radio-row">

								<div class="form-check">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="optionsRadios"
										       id="optionsRadios3"
										       value="option3">
										Quarterly Donation
									</label>
								</div>
								<div class="form-check">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="optionsRadios"
										       id="optionsRadios4"
										       value="option4">
										Annual Donation
									</label>
								</div>
							</div>
						</fieldset>
					</div> <!-- end donation -->
					<div class="form-group"> <!-- begin contact info -->
						<h2>Your Contact Information</h2>
						<p><a href="#">Our Privacy Policy</a></p>
						<div class="name-form">
							<input type="text" class="form-control" id="first-name" placeholder="First Name *" required>
							<input type="text" class="form-control" id="last-name" placeholder="Last Name *" required>
						</div>
						<input type="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
						       placeholder="Enter email *" required>
						<input type="text" class="form-control" id="phone-number" placeholder="Phone Number">

						<div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" value="">
								Check here if this is a corporate donation
							</label>
						</div>
					</div> <!-- end contact info -->

					<div class="form-group"> <!-- begin billing -->
						<h2>Your Billing Information</h2>
						<input type="text" class="form-control" id="cc-number" placeholder="Credit Card Number *"
						       required>
						<input type="text" class="form-control" id="cvv" placeholder="CVV *" required>
						<div class="cc-info">
							<select class="form-control" id="expire-month" required>
								<option value="" selected disabled>Expire Month *</option>

								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>

							<select class="form-control" id="expire-year" required>
								<option value="" selected disabled>Expire Year *</option>

								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>
						</div>
						<input type="text" class="form-control" id="address" placeholder="Billing Street Address *"
						       required>

						<input type="text" class="form-control" id="address2"
						       placeholder="Street Address 2 (Apt or Suite Number) *" required>

						<input type="text" class="form-control" id="billing-city" placeholder="Billing City *" required>

						<div class="billing-info">
							<select class="form-control" id="billing-state" required>
								<option value="" selected disabled>Billing State *</option>

								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>
							<input type="text" class="form-control" id="billing-zip" placeholder="Billing Zip *"
							       required>

						</div>

					</div> <!-- end billing -->


					<button type="submit" class="btn btn-primary">Submit</button>
				</form>

			</div> <!-- end form right -->
		</div>
	</div>
</section> <!-- end main -->
<footer class="footer-donation"> <!-- begin footer -->
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="footer-items">
					<div class="footer-logo">
						<img src="/images/donation/footer-logo.png">
					</div>
					<div class="footer-menu">
						<ul>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
							<li><a href="#">State Funding</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer> <!-- end footer -->
</body>
</html>
 




