<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
	      integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
		  <!-- Latest compiled and minified CSS -->
	<link href="/style.min.css" rel="stylesheet">
</head>
<body>
<script src="https://use.fontawesome.com/711fd3d519.js"></script>
<!-- Optional JavaScript -->
<!--jQuery-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!--Modernizr-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<!--Preload-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/PreloadJS/0.6.0/preloadjs.min.js"></script>

<!--Google Maps-->
<script type="text/javascript"
        src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCVmQ0BZztVQ-jVLAkDNcogmaY5Dw0gnJs'></script>

<!--Custom JS-->
<script type="text/javascript" src="/assets/js/custom.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
        integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
        crossorigin="anonymous"></script>

<header class="header-donation"> <!-- begin header -->
	<div class="container">

		<div class="row">
			<div class="col-7 col-md-4 col-lg-3">
				<div class="donation-logo-container">
					<a href="/"><img class="img-fluid" src="/images/donation/feed-logo.png"></a>
				</div>
			</div>
		</div>
	</div>
</header> <!-- end header -->
<section class="hero-donation "> <!-- begin hero -->
</section> <!-- end hero -->
<section class="main-donation"> <!-- begin main -->
	<div class="container">
		<div class="row">
			<div class="donoContent col-sm-12 col-md-12 col-lg-5"> <!-- main left -->
				<div class="left-col-item mt-4">
					<h2>Effectiveness and Efficiency</h2>
					<div class="left-col-inner">
						<img class="img-fluid multiGiftImg" src="/images/donation/7x-icon.png">
						<p class="multiGiftText">Each donated dollar helps provide seven dollars worth of food, essentials, and more to hungry
							children and families.</p>
					</div>
				</div>
				<br>
				<div class="left-col-item mt-4">
					<h2>Good Stewardship</h2>
					<p>In FY2016, 93% of our expenditures went to Program Services.</p>
					<img class="img-fluid stewardshipImg" src="/images/donation/93-percent-graph.png">
					<p>Feed the Children is 501(c)(3) non-profit organization; contributions are tax deductible as
						allowed by law.</p>
				</div>
				<br>
				<div class="left-col-item mt-4">
					<h2>Transparency and Trust</h2>
					<img class="bbbImg" src="/images/donation/BBB.png">
					<img class="digicert" src="/images/donation/digicert-icon.png">
				</div>
			</div> <!-- end main left -->
			<div class="form-container col-sm-12 col-md-12 col-lg-7"> <!-- form right -->
			<br>
				<form>
					<div class="form-group"> <!-- begin donation -->
						<h2>Help Us to Feed Hungry Children in Need
						</h2>
						<p><strong>This is a placeholder area for short variable copy that can be easily edited based on
								designation needs.</strong></p>
						<p>This is additional placeholder body copy that can be easily edited based on designation
							needs. </p>
						<div class="lock">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</div>




						<fieldset class="form-group">

							<h2>Donation Amount</h2>

					<div class="form-group"> <!-- begin donation -->

								<fieldset class="form-group">

									<div class="help-form-item donoP">
										<input type="text" class="form-control" id="donation-25"
											   placeholder="$25.00" readonly>

										<input type="text" class="form-control" id="donation-50"
											   placeholder="$50.00" readonly>

										<input type="text" class="form-control" id="donation-100"
											   placeholder="$100.00" readonly>
									</div>

									<div class="help-form-item donoP">

									<input type="text" class="form-control" id="donation-150"
											   placeholder="$250.00" readonly>

										<input type="text" class="form-control" id="donation-500"
											   placeholder="$500.00" readonly>

										<input class="form-control other-amt" type="text"
											   placeholder="Other">
									</div>
									<p class="pt-0 minimum-msg minDon"><span>$10 minimum</span></p>



									<div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input dedicateSomeone" type="checkbox" value="">
								Dedicate this donationto someone special
								<a href="#" class="helpLink" data-toggle="tooltip" data-placement="top" title=""
								data-original-title="placeholder text for donation">[?]</a>
							</label>
						</div>

							<fieldset class="form-group hidden" id="help-now-dedicate-fields">
								<div class="d-flex">
									<select class="col-sm-12 col-md-4 bg-white form-control">
										<option>In Honor Of</option>
										<option>In Memory Of</option>
									</select>
									<div class="col-sm-12 col-md-8">
									<input class="form-control" type="text" placeholder="Enter Name">
								  </div>
								</div>
							</fieldset>

								</fieldset>




							</div> <!-- end donation -->


<br>

							<h2>Donation Type</h2>


							<div class="radio-row">
								<div class="form-check">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="optionsRadios"
										       id="optionsRadios1"
										       value="option1" checked>
										One Time
									</label>
								</div>
								<div class="form-check">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="optionsRadios"
										       id="optionsRadios2"
										       value="option2">
										Monthly
									</label>
								</div>
							</div>
							<div class="radio-row">

								<div class="form-check">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="optionsRadios"
										       id="optionsRadios3"
										       value="option3">
										Quarterly
									</label>
								</div>
								<div class="form-check">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="optionsRadios"
										       id="optionsRadios4"
										       value="option4">
										Annual
									</label>
								</div>
							</div>
						</fieldset>
					</div> <!-- end donation -->
					<br>
					<div class="form-group"> <!-- begin contact info -->
						<h2>Contact Information</h2>
						<p><a href="/privacy-policy/">Our Privacy Policy</a></p>
						<div class="name-form">
							<input type="text" class="form-control" id="first-name" placeholder="First Name *" required>
							<input type="text" class="form-control" id="last-name" placeholder="Last Name *" required>
						</div>
						<input type="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
						       placeholder="Enter email *" required>
						<input type="text" class="form-control" id="phone-number" placeholder="Phone Number">

						<div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input dedicateCorporate" type="checkbox" value="">
								Check here if this is a corporate donation <a href="#" class="helpLink" data-toggle="tooltip" data-placement="top" title=""
								data-original-title="placeholder text for donation">[?]</a>
							</label>
						</div>
						<fieldset class="form-group hidden" id="help-now-dedicate-corporate-fields">
								<div class="d-flex">

								<input class="form-control"  style="margin-right:5px" type="text" placeholder="Company Name">


									<input class="form-control" style="margin-left:5px" type="text" placeholder="Company Website">

								</div>
							</fieldset>
					</div> <!-- end contact info -->
					<br>


					<script>
	$(".dedicateSomeone").change(function() {
        $("#help-now-dedicate-fields").toggle();
});

$(".dedicateCorporate").change(function() {
        $("#help-now-dedicate-corporate-fields").toggle();
});
</script>

					<div class="form-group"> <!-- begin billing -->
						<h2>Billing Information</h2>
						<input type="text" class="form-control" id="cc-number" placeholder="Credit Card Number *"
						       required>
						<input type="text" class="form-control" id="cvv" placeholder="CVV *" required>
						<div class="cc-info">
							<select class="form-control" id="expire-month" required>
								<option value="" selected disabled>Expire Month *</option>

								<option value='1'>Janaury</option>
								<option value='2'>February</option>
								<option value='3'>March</option>
								<option value='4'>April</option>
								<option value='5'>May</option>
								<option value='6'>June</option>
								<option value='7'>July</option>
								<option value='8'>August</option>
								<option value='9'>September</option>
								<option value='10'>October</option>
								<option value='11'>November</option>
								<option value='12'>December</option>
							</select>

							<select class="form-control" id="expire-year" required>
								<option value="" selected disabled>Expire Year *</option>

								<option value="2018">2018</option>
								<option value="2019">2019</option>
								<option value="2020">2020</option>
								<option value="2021">2021</option>
								<option value="2022">2022</option>
								<option value="2023">2023</option>
								<option value="2024">2024</option>
							</select>
						</div>
						<input type="text" class="form-control" id="address" placeholder="Billing Street Address *"
						       required>

						<input type="text" class="form-control" id="address2"
						       placeholder="Street Address 2 (Apt or Suite Number) *" required>

						<input type="text" class="form-control" id="billing-city" placeholder="Billing City *" required>

						<div class="billing-info">
							<select class="form-control" id="billing-state" required>
								<option value="" selected disabled>Billing State *</option>

								</option><option value="Alabama">Alabama</option><option value="Alaska">Alaska</option><option value="Arizona">Arizona</option><option value="Arkansas">Arkansas</option><option value="California">California</option><option value="Colorado">Colorado</option><option value="Connecticut">Connecticut</option><option value="Delaware">Delaware</option><option value="District of Columbia">District of Columbia</option><option value="Florida">Florida</option><option value="Georgia">Georgia</option><option value="Guam">Guam</option><option value="Hawaii">Hawaii</option><option value="Idaho">Idaho</option><option value="Illinois">Illinois</option><option value="Indiana">Indiana</option><option value="Iowa">Iowa</option><option value="Kansas">Kansas</option><option value="Kentucky">Kentucky</option><option value="Louisiana">Louisiana</option><option value="Maine">Maine</option><option value="Maryland">Maryland</option><option value="Massachusetts">Massachusetts</option><option value="Michigan">Michigan</option><option value="Minnesota">Minnesota</option><option value="Mississippi">Mississippi</option><option value="Missouri">Missouri</option><option value="Montana">Montana</option><option value="Nebraska">Nebraska</option><option value="Nevada">Nevada</option><option value="New Hampshire">New Hampshire</option><option value="New Jersey">New Jersey</option><option value="New Mexico">New Mexico</option><option value="New York">New York</option><option value="North Carolina">North Carolina</option><option value="North Dakota">North Dakota</option><option value="Northern Marianas Islands">Northern Marianas Islands</option><option value="Ohio">Ohio</option><option value="Oklahoma">Oklahoma</option><option value="Oregon">Oregon</option><option value="Pennsylvania">Pennsylvania</option><option value="Puerto Rico">Puerto Rico</option><option value="Rhode Island">Rhode Island</option><option value="South Carolina">South Carolina</option><option value="South Dakota">South Dakota</option><option value="Tennessee">Tennessee</option><option value="Texas">Texas</option><option value="Utah">Utah</option><option value="Vermont">Vermont</option><option value="Virginia">Virginia</option><option value="Virgin Islands">Virgin Islands</option><option value="Washington">Washington</option><option value="West Virginia">West Virginia</option><option value="Wisconsin">Wisconsin</option><option value="Wyoming">Wyoming</option></select>
							</select>
							<input type="text" class="form-control" id="billing-zip" placeholder="Billing Zip *"
							       required>

						</div>

					</div> <!-- end billing -->


					<button type="submit" class="btn">Make This Donation</button>
				</form>

			</div> <!-- end form right -->
		</div>
	</div>
</section> <!-- end main -->
<footer class="footer-donation"> <!-- begin footer -->
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="footer-items">
					<div class="footer-logo">
						<img src="/images/donation/footer-logo.png">
					</div>
					<div class="footer-menu">
						<ul>
							<li><a href="/privacy-policy/">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
							<li><a href="#">State Funding</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer> <!-- end footer -->
<script>
	$( document ).ready(function() {
		var number = 1 + Math.floor(Math.random() * 3);
		var heroClass = "heroKid" + number;
		$(".hero-donation").addClass(heroClass);

});

</script>
</body>
</html>
