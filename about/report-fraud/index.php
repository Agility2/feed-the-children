<?php include( '../../head.php' ); ?>
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h1>REPORT FRAUD OR ABUSE</h1>
      <p>To ensure Feed the Children, Inc. conducts its business fairly, impartially, in an ethical and proper manner, and in compliance with all laws and regulations, the organization established a toll-free hotline in 2008. The hotline is staffed 24 hours a day, and is managed by an independent organization.</p>
      <p><strong>The hotline is available toll-free at: 1-866-614-4991</strong></p>
      <p>The confidential hotline is dedicated to submission of concerns by Directors, Officers, employees or volunteers of the organization regarding questionable accounting, internal controls, auditing matters and the prevention of illegal activity, on a confidential and anonymous basis. The following are some specific examples of incidents that may warrant a call to the hotline:</p>
      <p>
        <ul>
          <li>
          Theft or inappropriate removal or possession of property
          </li>
          <li>If someone is falsifying or tampering with Feed the Children records
          </li>
          <li>If someone offers or you become aware of a kickback (i.e., money and/or services)
          </li>
          <li>If someone is impeding or overstepping internal controls
          </li>
          <li>If you become aware of or witness illegal activity
          </li>
          <li>Feed the Children encourages directors, volunteers, and employees to raise concerns within the organization for investigation and appropriate action. With this goal in mind, no director, officer, employee or volunteer who, in good faith, reports a concern shall be subject to retaliation or, in the case of an employee, adverse employment consequences. Moreover, a volunteer or employee who retaliates against someone who has reported a concern in good faith is subject to discipline up to and including dismissal from the volunteer position or termination of employment.
          </li>
        </ul>
      </p>
      <p>Individuals who feel that they are being or have been retaliated against should immediately report this to the hotline at 1-866-614-4991.</p>
    </div>
  </div>
</div>
<?php include( '../../footer.php' ); ?>

<!-- This is to style the header for blank pages ( _global.scss ).  -->
<script type="text/javascript">
$(document).ready(function(){
  $('body').addClass('blank');
});
</script>
