<?php include( '../head.php' ); ?>
<div class="hero d-flex align-items-end"> <!--HERO-->
	<div class="container text-center text-lg-left">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-6">
				<h2>About Us</h2>
			</div>
		</div>
	</div>
</div> <!--end hero-->

<div class="about-us-wrapper">

	<section class="about-intro"> <!--intro-->
		<div class="container">
			<div class="row">
				<div class="about-intro-left col-sm-12 col-lg-6 d-flex flex-column mt-lg-4">
					<h2>It takes the power of many to end childhood hunger.</h2>
					<p>It takes donors who believe in the case, experts to diagnose the problem and innovate solutions, organizations to pool their resources and expertise, and communities to work together for change.</p>

						<p>Feed the Children exists to end childhood hunger. It’s the cause upon which we were founded in 1979 and the one that we continue to fight for each and every day.</p>

						<p>We know it takes the power of many to end childhood hunger for good. We connect donors, experts, partners, leaders and communities to attack the problem from all angles.</p>

						<p>We are taking a stand and we will not rest until every child has enough to eat.</p>
					<ul>
						<li>Donors who believe in the cause</li>
						<li>Experts to diagnose the problem and innovate solutions</li>
						<li>Organizations to pool their resources and expertise</li>
						<li>Communities to work together for change</li>
					</ul>
				</div>

				<div class="about-intro-right col-sm-12 col-lg-6">
					<!-- <img class="img-fluid" src="/images/about-us/we-help-kids-video.jpg"> -->
					<div class="youtube" data-embed="P34wV1OJ-MY">
						<div class="play-button"></div>
					</div>
					<div class="about-intro-right-item d-flex align-items-center justify-content-center flex-wrap">
						<div class="about-intro-copy col col-md-6">
							<h2 class="pt-md-3 pb-md-3">Responsible Stewardship</h2>
							<p>93% of Feed the Children's expenditures in FY2016 went to Program Services.</p>
						</div>
						<div class="about-intro-img col col-lg-6">
							<img class="img-fluid" src="/images/93-percent-graph.png">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> <!--intro-->

	<section class="our-mission"> <!--begin our mission-->
		<div class="container-fluid">
			<div class="row mission-row-one justify-content-center"> <!--begin row 1-->
				<div class="mission-image-item col-sm-6 col-md-6 col-lg-2">
					<img class="img-fluid" src="/images/about-us/456x470-mission-05.jpg">
				</div>
				<div class="mission-item col-sm-6 col-md-6 col-lg-2 d-flex justify-content-center flex-column"
				     id="mission-info">
					<div class="mission-inner d-flex flex-column justify-content-center">
						<h2>Our Mission</h2>
						<p>Providing hope and resources for those without life's essentials.</p>
					</div>
				</div>
				<div class="mission-image-item mission-image-2 col-sm-6 col-md-6 col-lg-2">
					<img class="img-fluid" src="/images/about-us/456x470-mission-04.jpg">
				</div>
				<div class="mission-item col-sm-6 col-md-6 col-lg-2 d-flex justify-content-center flex-column"
				     id="vision-info">
					<div class="mission-inner d-flex flex-column justify-content-center">
						<h2>Our Vision</h2>
						<p>Create a world where no child goes to bed hungry</p>
					</div>
				</div>
				<div class="mission-image-item hide-md col-sm-6 col-md-6 col-lg-2">
					<img class="img-fluid" src="/images/about-us/456x470-mission-03.jpg">
				</div>
			</div> <!--end row 1-->

			<div class="mission-row-two row justify-content-center"> <!--begin row 2-->
				<div class="mission-image-item col-sm-6 col-md-6 col-lg-2">
					<img class="img-fluid" src="/images/about-us/456x470-mission-02.jpg">
				</div>
				<div class="mission-image-item col-sm-12 col-md-12 col-lg-6">
					<img class="img-fluid" src="/images/about-us/1388x470-mission-01.jpg">
				</div>
				<div class="mission-image-item col-sm-6 col-md-6 col-lg-2">
					<img class="img-fluid" src="/images/about-us/456x470-mission-01.jpg">
				</div>
			</div> <!--end row 2-->
		</div>
	</section> <!--end our mission-->

	<section class="brand-values"> <!--begin brand values-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-lg-6">
					<h2>Our Values</h2>
					<p class="pt-3"><strong>Challenge convention: </strong> we believe that a future without hungry
						children
						is possible.
					</p>
					<p class="pt-3"><strong>Defend dignity: </strong>we believe in treating each child and family in the
						communities
						where we work with value and worth.</p>
					<p class="pt-3"><strong>Champion partnership: </strong>we believe collaboration is the only way to
						end
						childhood
						hunger.</p>
					<p class="pt-3"><strong>Value every donor: </strong> we believe in donors playing an active role in
						ending childhood
						hunger.</p>
					<p><strong>Drive accountability:</strong> we believe in making changes when something isn't working
						and
						building on the success when it is.</strong></p>
				</div>
				<div class="col-sm-12 col-lg-6">
					<h2>Our Story</h2>
					<p>Feed the Children exists to end childhood hunger, it's the cause upon which we were founded in
						1979
						and the one that we continue to fight for each and every day.</p>
					<p>We know it takes the power of many to end childhood hunger for good. We connect donors, experts,
						partners, leaders and communities to attach the problem from all angles.</p>
					<p>We are taking a stand and we will not rest until every child has enough to eat.</p>
					<a class="btn btn-primary" href="/about/leadership-team/" role="button">Our Leadership</a>
				</div>
			</div>
		</div>
	</section> <!--end brand values-->

	<section class="about-gifts"><!--begin gifts-->
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2>Where Your Gifts Go</h2>
					<p>Feed the Children works hard to ensure every dollar is used wisely to accomplish the most
						good.</p>
					<div class="row">
						<div class="about-gifts-lower-info col-sm-12 col-lg-6">
							<h2>Responsible Stewardship</h2>
							<p>93% of Feed the Children's expenditures in FY2016 went to Program Services.</p>
						</div>
						<div class="about-gifts-lower-image col-sm-12 col-lg-6">
							<img class="img-fluid" src="/images/about-us/7x-Icon-White.png">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> <!--end gifts-->

	<section class="stewarship-transparency">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
				<h2>Good Stewardship and Transparency</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-7 col-lg-8">
					<p>At Feed the Children, our pledge is to be effective in our our outreach to those in need by being
						good stewards of the funds entrusted to us. We are audited yearly, and the information below is
						summarized from the consolidated financial statement.</p>
				
				<p>Financial data presented here is summarized from the consolidated financial statements of Feed
						the
						Children Inc. for the fiscal year ended June 30, 2016. Contributions to Feed the Children are
						tax-deductible as allowed by law.
					<div class="about-links">
						<ul>
							<li><a href="/about/financial-statements/">Form 990</a></li>
							<li><a href="/about/financial-statements/">Consolidated Financial Statements</a></li>
							<li><a href="/about/financial-statements/">2016 Annual Report</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-12 col-md-5 col-lg-4">
					<img src="/images/about-us/BBB-Logo.jpg">
					<img src="/images/about-us/GuideStar-Logo.jpg">
				</div>
			</div>
		</div>
	</section>

	<section class="fraud">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-sm-12 col-md-6">
					<h2>Report Fraud or Abuse</h2>
					<p>To ensure Feed the Children Inc. conducts its busness fairly, impartially in an ethical and
						proper
						manner, and in compliance with all laws and regulations, the organization established a
						toll-free
						hotline in 2008.</p>

				</div>
				<div class="col-sm-12 col-md-6">
					<h4><strong>Hotline: 1-866-614-4991</strong></h4>
					<p>The hotline is staffed 24 hours a day and is managed by an independent firm. More information
						about
						the hotline is avaliable <a href="/about/report-fraud">here.</a></a></p>
				</div>
			</div>
		</div>

	</section>

	<section class="about-pre-footer text-center">
		<div class="container">
			<div class="row">
				<div class="col">
					<p class="mb-0">Feed the Children is a 501(c)(3) non-profit organization.</p>
					<p>Donations and contributor are tax deductible as allowed by law.</p>
				</div>
			</div>
		</div>
	</section>

</div>
<?php include( '../footer.php' ); ?>
