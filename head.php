<!DOCTYPE html>
<html lang="en">
<head>

	<title>Feed The Children</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Feed The Children">
	<meta name="twitter:card" content="summary">
	<meta name="twitter:description" content="Feed The Children">
	<meta name="twitter:title" content="Feed The Children">
	<meta property="og:locale" content="en_US">
	<meta property="og:type" content="website">
	<meta property="og:title" content="Feed The Children">
	<meta property="og:description" content="Feed The Children">
	<meta property="og:site_name" content="Feed The Children">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
	      integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<!-- FontAwesome CSS -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<!--Google Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="/style.min.css">
	<link rel="icon" href="/favicon.ico" type="image/x-icon"/>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>

</head>
<header class="global-header sticky"> <!-- begin header -->
	<div class="form-wrapper" style="display:none;"> <!-- begin search form -->
		<div class="esc-form"><span class="close-search">x</span></div>
		<form id="fast-search-desktop form-wrapper" class="form-wrapper" style="display:block !important;"
		      role="search" method="get" action="/">
			<div class="goSearch">
				<input class="popup-search-bar search" type="search" placeholder="Search" value="" name="s"
				       title="Search for:">
				<input type="Submit" value="Go">
				<!--            <a class="goSearch" href="javascript:;">Go</a>-->
			</div>
			<input type="hidden" name="post_type" value="product">
		</form>
	</div> <!-- end search form -->

	<nav class="navbar navbar-expand-xl navbar-light">
		<a class="navbar-brand desktop-logo" href="/"><img class="logo img-responsive"
		                                                   src="/images/FTC-Logo-White.png"></a>
		<a class="navbar-brand mobile-logo" href="/"><img class="logo img-responsive" src="/images/FTC-Logo-Color.png"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target=""
		        aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="/about/" id="navbarDropdownMenuLink">
						About Us
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="/about/financial-statements/">Financial Statements</a>
						<a class="dropdown-item" href="/about/board">Board of Directors</a>
						<a class="dropdown-item" href="/about/leadership-team">Leadership Team</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="/our-work/" id="navbarDropdownMenuLink">
						Our Work
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="/our-work/hunger-facts/">Hunger Facts and Figures</a>
						<a class="dropdown-item" href="/our-work/hunger-usa/">In the USA</a>
						<a class="dropdown-item" href="/our-work/around-the-world/">Around the World</a>
						<a class="dropdown-item" href="/our-work/disaster-response/">Disaster Response</a>
						<a class="dropdown-item" href="/our-work/operating-model/">Operating Model</a>
						<a class="dropdown-item" href="/our-work/stories/">Stories</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="/get-involved/" id="navbarDropdownMenuLink">
						Get Involved
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="/donate/">Donate</a>
						<a class="dropdown-item" href="/get-involved/sponsor-a-child/">Sponsor a Child</a>
						<a class="dropdown-item" href="/get-involved/sponsor-a-truck/">Sponsor a Truck</a>
						<a class="dropdown-item" href="/get-involved/corporate-partners/">Corporate Partnership</a>
						<a class="dropdown-item" href="/get-involved/planned-giving/">Planned Giving</a>
						<a class="dropdown-item" href="/get-involved/social-sharing/">Social Sharing</a>
						<a class="dropdown-item" href="/get-involved/volunteer/">Volunteer</a>
						<a class="dropdown-item" href="/get-involved/advocate/">Advocate</a>
						<a class="dropdown-item" href="/get-involved/other-ways-to-give/">Other Ways to Give</a>

					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link btn btn-featured donate pr-3 pl-3" href="/donate/">Donate</a>
				</li>
				<li class="nav-item">
					<a class="nav-link search-header pt-xl-0" href="#"><i class="fa fa-search"></i></a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle pt-xl-0" href="/login/"><i class="fa fa-user"></i></a>

					<div class="dropdown-menu account" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="/my-account/">My Account</a>
						<a class="dropdown-item" href="/my-account/personal-information">Personal Information</a>
						<a class="dropdown-item" href="/my-account/transaction-history">Transaction History</a>
						<a class="dropdown-item" href="/my-account/billing-information">Billing Information</a>
						<a class="dropdown-item" href="/my-account/my-sponsorships">My Sponsorships</a>
						<a class="dropdown-item" href="/my-account/my-monthly-gifts">My Monthly Gifts</a>
						<a class="dropdown-item" href="/">Logout</a>
					</div>

				</li>
			</ul>
		</div>
	</nav>

	<div id="mobileMenu">
		<nav>
			<ul class="list-unstyled main-menu">
				<div class="menu-mobile-menu-container">
					<li class="menu-item-has-children"><a href="/about/">About Us</a>
						<ul class="sub-menu">
							<li><a href="/about/financial-statements/">Financial Statements</a></li>
							<li><a href="/about/board">Board of Directors</a></li>
							<li><a href="/about/leadership-team">Leadership Team</a></li>
						</ul>
					</li>
					<li><a href="/our-work/">Our Work</a>
						<ul class="sub-menu">
							<li><a href="/our-work/hunger-facts/">Hunger Facts and Figures</a></li>
							<li><a href="/our-work/hunger-usa/">In the USA</a></li>
							<li><a href="/our-work/around-the-world/">Around the World</a></li>
							<li><a href="/our-work/disaster-response/">Disaster Response</a></li>
							<li><a href="/our-work/operating-model/">Operating Model</a></li>
							<li><a href="/our-work/stories/">Stories</a></li>
						</ul>
					</li>
					<li><a href="/get-involved/">Get Involved</a>
						<ul class="sub-menu">
							<li><a href="/donate/">Donate</a></li>
							<li><a href="/get-involved/sponsor-a-child/">Sponsor a Child</a></li>
							<li><a href="/get-involved/sponsor-a-truck/">Sponsor a Truck</a></li>
							<li><a href="/get-involved/corporate-partners/">Corporate Partnership</a></li>
							<li><a href="/get-involved/planned-giving/">Planned Giving</a></li>
							<li><a href="/get-involved/social-sharing/">Social Sharing</a></li>
							<li><a href="/get-involved/volunteer/">Volunteer</a></li>
							<li><a href="/advocate/">Advocate</a></li>
							<li><a href="/get-involved/other-ways-to-give/">Other Ways to Give</a></li>
						</ul>
					</li>
					<li class="donation-link"><a href="/donate">Make a Donation <span><i class="fa fa-lock"
					                                                                     aria-hidden="true"></i></span></a>
					</li>
				</div>

			</ul>
			<ul class="list-unystyled utility-menu row">
				<li class="col-6"><a href="/my-account/"><span><i class="fa fa-user"></i></span><span>My Account</span></a></li>
				<li class="col-6"><a href="#"><span><i class="fa fa-search"></i></span> <span>Search</span></a></li>
			</ul>

		</nav>

	</div>

	<div class="overlay-form hidden-lg-down"> <!--search form pop up-->
		<div class="overlay-inner">
			<form role="search" method="get" action="/">
				<input type="text" name="s" placeholder="Search the Site"/>
				<input type="submit" value="Search" class="btn"/>
			</form>
			<div class="esc-form"><a href="javascript:;" class="close-search">x</a></div>
		</div>
	</div> <!--end search form popup-->

</header> <!-- end header -->
<body>
<!-- Optional JavaScript -->
<!--jQuery-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!--Modernizr-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<!--Preload-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/PreloadJS/0.6.0/preloadjs.min.js"></script>

<!--Google Maps-->
<script type="text/javascript"
        src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCVmQ0BZztVQ-jVLAkDNcogmaY5Dw0gnJs'></script>

<!--Custom JS-->
<script type="text/javascript" src="/assets/js/custom.min.js"></script>

<!--Font Awesome-->
<script src="https://use.fontawesome.com/711fd3d519.js"></script>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
        integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
        crossorigin="anonymous"></script>

<!--Search function-->
<script>
	(function ($) {

		$(document).ready(function () {
			$('.esc-form a').click(function () {
				$('.overlay-form').slideUp(100);
			});

			$('.search-header').click(function () {
				$('.overlay-form').slideDown(100);
			});

		});


		$('ul.navbar-nav li.dropdown').hover(function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(300);
		}, function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(300);
		});


		$(document).ready(function () {
			//Navigation Menu Slider
			$('.navbar-toggler').on('click', function (e) {
				e.preventDefault();
				$('header').toggleClass('nav-expanded');
			});

			// Initialize navgoco with default options

			$(".main-menu").navgoco({
				caretHtml: '<i class="fa fa-caret-left" aria-hidden="true"></i>',
				accordion: false,
				openClass: 'open',
				save: true,
				cookie: {
					name: 'navgoco',
					expires: false,
					path: '/'
				},
				slide: {
					duration: 400,
					easing: 'swing'
				},
			});
		});

	})(jQuery);


</script>
