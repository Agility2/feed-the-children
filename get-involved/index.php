  <?php include( '../head.php' ); ?>
<div id="getInvolved">
  <div class="getInHero d-flex align-items-end"> <!--HERO-->

        <div class="container">
          <div class="row">
            <div class="col-12">
                    <div class="heroContent">
										<h2>Get Involved</h2>
                    </div>
            </div>
          </div>
        </div>
  </div> <!-- end of hero -->

	<div class="otherWays">
   <div class="TWLA-services-section-2">
				<div class="container TWLA-full-width">

					<div class="row TWLA-row">
						<div class="TWLA-services col-lg-12 col-md-12 col-sm-12 col-12">

							<div class="TWLA-unit col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
								<div class="TWLA-cube">
									<div class="TWLA-unit-wrap-default">
										<div class="hunger-image"><img src="../images/get-involved/corporate-donations-icon.png">
										</div>
										<div class="hunger-caption">
											<div class="hunger-head">CORPORATE DONATIONS</div>
											<span class="TWLA-gradient"></span>
										</div>
									</div>
									<div class="TWLA-unit-wrap-hover">
										<div class="hunger-side">
											<h5 class="font-weight-bold">CORPORATE DONATIONS</h5>
											<p>We work with corporations of all sizes for financial donations, in-kind donations and other partnership opportunities. .</p>
											<a class="btn btn-primary" href="/get-involved/corporate-partners/">Let's Partner</a>
										</div>
										<span class="TWLA-gradient"></span>
									</div>
								</div>
							</div>

							<div class="TWLA-unit col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
								<div class="TWLA-cube">
									<div class="TWLA-unit-wrap-default">
										<div class="hunger-image"><img src="../images/get-involved/stocks.png">
										</div>
										<div class="hunger-caption">
											<div class="hunger-head">STOCKS</div>
											<span class="TWLA-gradient"></span>
										</div>
									</div>
									<div class="TWLA-unit-wrap-hover">
										<div class="hunger-side">
											<h5 class="font-weight-bold">STOCKS</h5>
											<p>Your gift of appreciated securities, including stocks or bonds, is a simple and effective way to change the lives of children in need.</p>
											<a class="btn btn-primary" href=" /get-involved/other-ways-to-give#stock">Planned Giving</a>
										</div>
										<span class="TWLA-gradient"></span>
									</div>
								</div>
							</div>

							<div class="TWLA-unit col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
								<div class="TWLA-cube">
									<div class="TWLA-unit-wrap-default">
										<div class="hunger-image"><img src="../images/get-involved/estate-giving.png">
										</div>
										<div class="hunger-caption">
											<div class="hunger-head">ESTATE GIVING</div>
											<span class="TWLA-gradient"></span>
										</div>
									</div>
									<div class="TWLA-unit-wrap-hover">
										<div class="hunger-side">
											<h5 class="font-weight-bold">ESTATE GIVING</h5>
											<p>OAppreciated real property — like a home, vacant land or commercial property can be a tremendous contribution to our mission.</p>
											<a class="btn btn-primary" href="/get-involved/other-ways-to-give#estate">Estate Donations</a>
										</div>
										<span class="TWLA-gradient"></span>
									</div>
								</div>
							</div>

							<div class="TWLA-unit col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
								<div class="TWLA-cube">
									<div class="TWLA-unit-wrap-default">
										<div class="hunger-image"><img src="../images/get-involved/workplace-giving.png">
										</div>
										<div class="hunger-caption">
											<div class="hunger-head">WORKPLACE GIVING & MATCHING GIFTS</div>
											<span class="TWLA-gradient"></span>
										</div>
									</div>
									<div class="TWLA-unit-wrap-hover">
										<div class="hunger-side">
											<h5 class="font-weight-bold">WORKPLACE GIVING & MATCHING GIFTS</h5>
											<p>Workplace giving is one of the easiest and most effective ways to support the causes that are closest to you. </p>
											<a class="btn btn-primary" href="/get-involved/other-ways-to-give#workplace">Ask Your Boss</a>
										</div>
										<span class="TWLA-gradient"></span>
									</div>
								</div>
							</div>

							<div class="TWLA-unit col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
								<div class="TWLA-cube">
									<div class="TWLA-unit-wrap-default">
										<div class="hunger-image"><img
												src="../images/get-involved/vehicle-donations.png">
										</div>
										<div class="hunger-caption">
											<div class="hunger-head">VEHICLE DONATIONS</div>
											<span class="TWLA-gradient"></span>
										</div>
									</div>
									<div class="TWLA-unit-wrap-hover">
										<div class="hunger-side">
											<h5 class="font-weight-bold">VEHICLE DONATIONS</h5>
											<p>Donating your car, truck, boat, motorcycle, ATV, RV, trailer or other vehicle to Feed the Children is easy and has many benefits.</p>
											<a class="btn btn-primary" href=" /get-involved/other-ways-to-give#vehicle">Donate a Vehicle</a>
										</div>
										<span class="TWLA-gradient"></span>
									</div>
								</div>
							</div>

							<div class="TWLA-unit col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
								<div class="TWLA-cube">
									<div class="TWLA-unit-wrap-default">
										<div class="hunger-image"><img src="../images/get-involved/mobile-giving.png">
										</div>
										<div class="hunger-caption">
											<div class="hunger-head">MOBILE GIVING</div>
											<span class="TWLA-gradient"></span>
										</div>
									</div>
									<div class="TWLA-unit-wrap-hover">
										<div class="hunger-side">
											<h5 class="font-weight-bold">MOBILE GIVING</h5>
											<p>We have a number of quick and easy ways too donate to the cause on-the-go through your mobile phone provider.</p>
											<a class="btn btn-primary" href="/get-involved/other-ways-to-give#mobile">Text to Give</a>
										</div>
										<span class="TWLA-gradient"></span>
									</div>
								</div>
							</div>


						</div>

					</div>
				</div>
			</div>



</div>
  <div class="bSpots">
  <div class="container">
    <div class="row">
			
        <div class="col-md-6">
				<form action="/donate"> 
            <div class="joinFight">
              <h2><strong>Join the fight to end hunger.</strong></h2>
              <p>When you feed a child, you feed her future. You make the world a better place for her and for each one of us when you give to provide much-needed food and other essentials.</p>
            </div>
            <div class="donationForm">
              <h2><strong>Choose Your Donation</strong></h2>
              <div class="row">
                <div class="col-md-6">
                <div class="form-check">
                    <label class="form-check-label">
                      <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                      One Time Donation
                    </label>
                  </div>
                </div>
                <div class="col-md-6">
                <div class="form-check">
                    <label class="form-check-label">
                      <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                      Quarterly Donation
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                <div class="form-check">
                    <label class="form-check-label">
                      <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                      Monthly Donation
                    </label>
                  </div>
                </div>
                <div class="col-md-6">
                <div class="form-check">
                    <label class="form-check-label">
                      <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                      Annual Donation
                    </label>
                  </div>
                </div>
								<button type="submit" class="btn btn-primary giBtn" style="margin-top:20px; margin-left:18px;"> Donate Now</button>
              
              </div>
            </div>
			</form>
					</div>
				
        <div class="col-md-12 col-lg-6">
          <div class="responsible">
            <div class="row">
            <div class="col-md-4">
              <h3><strong>Responsible Stewardship</strong></h3>
              <p>We operate efficiently to make your donations go further.</p>
            </div>
            <div class="col-md-8">
              <img src="../../images/get-involved/pie-graph.png">
            </div>
            </div>
          </div>
        </div>
    </div>
  </div>
  </div>
<div class="row no-gutters">
  <div class="col-md-12 col-lg-6">
    <img class="scImg" src="../../images/get-involved/1280x800-sponsor-child.jpg">
  </div>
  <div class="col-md-12 col-lg-6 sponsorChild">
    <div class="sponsorChildContent">
      <h2><strong>Sponsor a Child</strong></h2>
      <p>As a sponsor, you’ll protect a child from the enemies of childhood — hunger, disease and poverty. You’ll meet urgent physical needs and provide hope for the future.</p>
 <p>Name: <strong>Renee</strong></p>
 <p>Age: <strong>4 Years</strong></p>
 <p>Country: <strong>Guatemala</strong></p>
 <p>Waiting: <strong>Guatemala</strong></p>
 <a href="/get-involved/sponsor-a-child/" class="btn btn-primary">SPONSOR ME </a>
</div>
  </div>
</div>



<div class="giveToFeed">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="gtfContent">
          <h2>Give to Feed American's Children</h2>
          <p>Millions of children are going hungry right here in America. They need your help. A monthly gift of $32 provides food, hygiene items and school supplies to kids in need across the United States.</p>
          <a href="/donate/" class="btn btn-primary">HELP NOW</a>
        </div>
      </div>
      <div class="col-md-6">
        <img class="gtfImg" src="../../images/get-involved/855x550-give-to-feed.jpg" />
      </div>
    </div>
  </div>
</div>

<div class="waysTohelp">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <img src="../../images/get-involved/volunteer.jpg">
        <p>Give your time by volunteering in your community to help make sure no child goes to bed hungry.</p>
        <a href="/get-involved/volunteer/">Search Opportunities</a>
      </div>
      <div class="col-md-6">
      <img src="../../images/get-involved/advocate.jpg">
        <p>Speak up for boys and girls in need when you champion hunger-fighting efforts.</p>
        <a href="/get-involved/advocate/">Start Advocating</a>
      </div>
    </div>
  </div>
</div>

</div> <!-- end of #getInvolved -->

 <?php include( '../footer.php' ); ?>
