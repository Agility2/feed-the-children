<?php include( '../../head.php' ); ?>
<!--HERO-->
<div class="interior-hero corporate-partners-hero d-flex align-items-end">
<div class="container text-center text-lg-left">
<div class="row">
	<div class="col-12 col-md-12 col-lg-6">
		<h2>Corporate Partners</h2>
	</div>
</div>
</div>
</div>
<!--end hero-->
<div class="container mb-5">
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-6"> <!-- main left -->
			<div class="left-col-item mt-4">
				<h2>Your Gift Has Wings</h2>
				<div class="left-col-inner">
					<p>Feed the Children would love to partner with your organization. Our fleet of trucks and
						distribution centers around the United States allow us to pick up your donated goods and get
						them to kids in need quickly and efficiently. And it's all done at no cost to your company.</p>
				</div>
			</div>
			<div class="left-col-item mt-2">
				<h3>Types of Partnerships</h3>
				<ul>
					<li>Financial Donation</li>
					<li>In-Kind Donation</li>
					<li>Fundraiser Help</li>
					<li>Lorem Ipsum</li>
					<li>Dolor Sit Amet</li>
					<li>Arcme Han Jirem</li>
				</ul>
			</div>
		</div> <!-- end main left -->
		<div class="corporate-partners-form col-sm-12 col-md-12 col-lg-6"> <!-- form right -->
			<br>
			<form>
				<div class="form-group"> <!-- begin contact info -->
					<h2>Contact Donor Relations</h2>
					<p>This is placeholder area for short variable copy that can be easily edited based on designation needs.</p>
					<h3>Your Contact Information</h3>
					<p><a href="/privacy-policy/">Our Privacy Policy</a></p>
					<div class="name-form">
						<input type="text" class="form-control" id="first-name" placeholder="First Name *" required="">
						<input type="text" class="form-control" id="last-name" placeholder="Last Name *" required="">
					</div>
					<input type="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
					       placeholder="Enter email *" required="">
					<input type="text" class="form-control" id="phone-number" placeholder="Phone Number">
					<select class="form-control">
						<option>Desired Partnership</option>
					</select>
				</div> <!-- end contact info -->
				<button type="submit" class="btn">Submit</button>
			</form>

		</div> <!-- end form right -->
	</div>
</div>


<section class="contributions"> <!--begin contributions-->
	<div class="container pb-5 pt-5">
		<div class="row align-items-center justify-content-center">
			<div class="col-12 col-md-6">
				<div class="contributions-left">
					<h2>Your Contributions Matter</h2>
					<p>Donating goods are not the only way you can help us feed hungry children! Any amount of funds you
						donate will be used to help put food onto dinner tables around the US and the world.</p>
					<a class="btn btn-featured" href="/donate">Donate</a>
				</div>
			</div>

			<div class="col-12 col-md-6">
				<img class="img-fluid" src="/images/corporate-partners/globe.png">
			</div>
		</div>
	</div>
</section> <!--end contributions-->

<section class="partners-slider"> <!--begin partners slider-->
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-6">
				<div class="partners-slide">
					<div class="slide-item">
						<div class="slide-item-inner d-flex col-12">
							<div class="col-12 col-lg-6">
								<img class="img-fluid" src="/images/corporate-partners/pepsico-logo.png">
							</div>
							<div class="col-12 col-lg-6">
								<img class="img-fluid" src="/images/corporate-partners/price-rite-logo.png">
							</div>
						</div>

						<div class="slide-item-inner d-flex col-12">
							<div class="col-12 col-lg-6">
								<img class="img-fluid" src="/images/corporate-partners/starkist-logo.png">
							</div>
							<div class="col-12 col-lg-6">
								<img class="img-fluid" src="/images/corporate-partners/toms-logo.png">
							</div>
						</div>
					</div>
					<div class="slide-item">
						<div class="slide-item-inner d-flex col-12">
							<div class="col-12 col-lg-6">
								<img class="img-fluid" src="/images/corporate-partners/pepsico-logo.png">
							</div>
							<div class="col-12 col-lg-6">
								<img class="img-fluid" src="/images/corporate-partners/price-rite-logo.png">
							</div>
						</div>

						<div class="slide-item-inner d-flex col-12">
							<div class="col-12 col-lg-6">
								<img class="img-fluid" src="/images/corporate-partners/starkist-logo.png">
							</div>
							<div class="col-12 col-lg-6">
								<img class="img-fluid" src="/images/corporate-partners/toms-logo.png">
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-12 col-lg-6 d-flex flex-column justify-content-center mt-4 mt-lg-0">
				<h2>Our Partners</h2>
				<p>Lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>
				<p>lorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem
					ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>
			</div>
		</div>
	</div>
</section> <!--end partners slider-->

<section class="philanthropy mb-5 mt-5">
	<div class="container-fluid pr-md-0">
		<div class="row">
			<div class="philanthropy-left-container col-12 col-xl-6 pl-xl-0">
				<div class="philanthropy-left d-flex align-items-center flex-column justify-content-center">
					<h2>Your Philanthropy Can Make a Difference</h2>
					<p><strong>Hungry children directly benefit from your gift - </strong>millions of them! We
						distribute
						food and other supplies to children in need and their families throughout the United States and
						around the world.</p>
					<p>We feature our gift-in-kind partners in scoial media and other marketing, giving your company
						recognition for its generosity.</p>
				</div>
			</div>


			<div class="philanthropy-right-container col-12 col-xl-6 pr-0 mt-3 mt-xl-0">
				<div class="philanthropy-right d-flex flex-wrap ml-0">
					<div class="col-12 col-md-6 pl-md-0 pr-md-0">
						<div class="middle-image">
						</div>
					</div>
					<div class="col-12 col-md-6 pt-4 pt-md-0">
						<img class="img-fluid" src="/images/corporate-partners/corporate-partners-collage-img-02.jpg">
						<img class="img-fluid pt-3"
						     src="/images/corporate-partners/corporate-partners-collage-img-03.jpg">
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<section class="corporate-why-partner mb-5"> <!--begin why partner section-->
	<div class="container"> <!--begin container-->
		<div class="row">
			<div class="col-12">
				<h2 class="text-center">Why Partner with Us?</h2>
				<p class="text-center">Donating to Feed the Children benefits your company in a number of ways</p>
			</div>
		</div>
		<div class="row"> <!--begin row-->
			<div class="why-item col-12 col-sm-6"> <!--item 1-->
				<div class="why-inner d-flex flex-wrap align-items-center">
					<div class="why-left col-12 col-sm-4 col-md-3">
						<img class="img-fluid" src="/images/corporate-partners/gift-with-wings.jpg">
					</div>
					<div class="why-right col-12 col-sm-8 col-md-9">
						<h4 class="font-weight-bold">Your Gift Has Wings</h4>
						<p>We can ensure your donation is distributed responsibly</p>
					</div>
				</div>
			</div> <!--end item 1-->

			<div class="why-item col-12 col-sm-6"> <!--item 2-->
				<div class="why-inner d-flex flex-wrap align-items-center">
					<div class="why-left col-12 col-sm-4 col-md-3">
						<img class="img-fluid" src="/images/corporate-partners/accountability.jpg">
					</div>
					<div class="why-right col-12 col-sm-8 col-md-9">
						<h4 class="font-weight-bold">Accountability</h4>
						<p>Our systems tell you where your product went and who benefited</p>
					</div>
				</div>
			</div> <!--end item 1-->

			<div class="why-item col-12 col-sm-6"> <!--item 3-->
				<div class="why-inner d-flex flex-wrap align-items-center">
					<div class="why-left col-12 col-md-3 col-sm-4">
						<img class="img-fluid" src="/images/corporate-partners/peace-of-mind.jpg">
					</div>
					<div class="why-right col-12 col-sm-8 col-md-9">
						<h4 class="font-weight-bold">Peace of Mind</h4>
						<p>Nothing you donate to Feed the Children will be sold elsewhere</p>
					</div>
				</div>
			</div> <!--end item 1-->

			<div class="why-item col-12 col-sm-6"> <!--item 3-->
				<div class="why-inner d-flex flex-wrap align-items-center">
					<div class="why-left col-12 col-md-3 col-sm-4">
						<img class="img-fluid" src="/images/corporate-partners/inventory.jpg">
					</div>
					<div class="why-right col-12 col-sm-8 col-md-9">
						<h4 class="font-weight-bold">Reduce Inventory Handling</h4>
						<p>Children and their families can use your unsealable and overages</p>
					</div>
				</div>
			</div> <!--end item 1-->

			<div class="why-item col-12 col-sm-6"> <!--item 3-->
				<div class="why-inner d-flex flex-wrap align-items-center">
					<div class="why-left col-12 col-md-3 col-sm-4">
						<img class="img-fluid" src="/images/corporate-partners/tax-savings.jpg">
					</div>
					<div class="why-right col-12 col-sm-8 col-md-9">
						<h4 class="font-weight-bold">Tax Savings</h4>
						<p>You can receive up to twice the cost of the products you donate</p>
					</div>
				</div>
			</div> <!--end item 1-->

			<div class="why-item col-12 col-sm-6"> <!--item 3-->
				<div class="why-inner d-flex flex-wrap align-items-center">
					<div class="why-left col-12 col-md-3 col-sm-4">
						<img class="img-fluid" src="/images/corporate-partners/quick-response.jpg">
					</div>
					<div class="why-right col-12 col-sm-8 col-md-9">
						<h4 class="font-weight-bold">Quick Response</h4>
						<p>We can pick up loads anywhere in the U.S. within 72 hours</p>
					</div>
				</div>
			</div> <!--end item 1-->

		</div> <!--end row-->
	</div> <!--end container-->
</section> <!--end why partner-->
<script>
	(function ($) {

		$('.partners-slide').slick({
			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
		});
	})(jQuery);
</script>
<?php include( '../../footer.php' ); ?>

