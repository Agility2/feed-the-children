<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
	      integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
		  <!-- Latest compiled and minified CSS -->
	<link href="/style.min.css" rel="stylesheet">
</head>
<body class="account-login">
<script src="https://use.fontawesome.com/711fd3d519.js"></script>

<header class="header-donation"> <!-- begin header -->
	<div class="container">

		<div class="row">
			<div class="col-7 col-md-4 col-lg-3">
				<div class="donation-logo-container">
					<a href="/"><img class="img-fluid" src="/images/donation/feed-logo.png"></a>
				</div>
			</div>
		</div>
	</div>
</header> <!-- end header -->
<section class="hero-donation "> <!-- begin hero -->
</section> <!-- end hero -->
<section class="main-donation"> <!-- begin main -->
	<div class="container">
		<div class="row">

			<div class="form-container col-sm-12 col-md-12 col-lg-6"> <!-- form right -->

				<div class="loginmodal-container">
					<h2>Login to Your Account</h2><br>
				  <form action="/my-account/">
					<input type="text" name="user" placeholder="Username">
					<input type="password" name="pass" placeholder="Password">
					<input type="submit" name="login" class="btn login loginmodal-submit" value="Login">
				  </form>

				  <div class="login-help">
					<a href="#">Register</a> - <a href="#">Forgot Password</a>
				  </div>
				</div>

			</div> <!-- end form right -->
		</div>
	</div>
</section> <!-- end main -->
<footer class="footer-donation"> <!-- begin footer -->
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="footer-items">
					<div class="footer-logo">
						<img src="/images/donation/footer-logo.png">
					</div>
					<div class="footer-menu">
						<ul>
							<li><a href="/privacy-policy/">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
							<li><a href="#">State Funding</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer> <!-- end footer -->
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
<script>
	$( document ).ready(function() {
		var number = 1 + Math.floor(Math.random() * 3);
		var heroClass = "heroKid" + number;
		$(".hero-donation").addClass(heroClass);

});

</script>
</body>
</html>
