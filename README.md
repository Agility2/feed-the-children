# Setup #

1.  In order to get the most up to date files,  run the command git clone https://Agility2@bitbucket.org/Agility2/feed-the-children.git from the command line.  This needs to be in whatever directory you are using that is holding your websites for local development.  Name the local install feed.dev.  
2.  Next you need to install node, node package manager, and gulp on your system.  If you already have these, you can skip this. 
https://nodejs.org/en/download/
3.  Next, go into the root of feed.dev and type the command "npm install".  This will install all packages.  
4.  Type "gulp" and you are set up to begin local development.  

### Page templates ###

All of the page templates are inside a directory that is named after the page.  For example, if you want edit the about page, you would go to "about/index.php" to edit this page.  The exception is the donation page.  This can be edited at donation.php.

### Images ###

All of the images being used are in the "/images" directory.  Most of the images being used in specific areas or on page templates are split into sub directories in this directory.  

### SCSS ###

In order to compile all of the SCSS, you must the command "gulp" from the command line.  All of the files being loaded in the "/assets/scss/style.scss", get compiled into the "style.css" and "style.min.css file".  The style.min.css file is the one being loaded into the pages.  
The partials in "/assets/scss/partials" are all named accordingly depending on the section they are targeting.  For example, "_global-footer.scss" is where all of the SCSS for the footer is written.   
If you are creating a new section or a new page, add your SCSS partials to the "assets/scss/style.scss" using the same structure that we are currently using in that file.  When naming these partials, try to be as specific as possible.  For a new page template, use the naming convention "_template-pagename.scss"


### JavaScript ###

All of the JS that is to be compiled is in "/assets/js/custom/".  You can add to "ftc.js" or add your own JS file into the "/custom" directory and it will be compiled into the "/assets/js/custom.min.jss".