
    <?php include( '../head.php' ); ?>
    <div class="container">

        <div class="row">
            <div class="col-12">
                <h1>Top Nav Site Map</h1>
            </div>
        </div>

    <div class="row">
        <div class="col-md-2">
        <h4><a href="/about">About</a></h4>
                 <a href="/about/financial-statements/">Financial Statements</a><br>
                <a href="/about/board">Board of Directors</a><br>
                <a href="/about/leadership-team">Leadership Team</a><br>
                <a href="/about/report-fraud">Report Fraud and Abuse</a><br>
        </div>
        <div class="col-md-2">
        <h4><a href="/our-work">Our Work</a></h4>
                <a href="/our-work/hunger-facts/">Hunger Facts and Figures</a><br>
                <a href="/our-work/hunger-usa/">In the USA</a><br>
                <a href="/our-work/around-the-world/">Around the World</a>
                <ul>
                    <li><a href="/our-work/around-the-world/el-salvador">El Salvador</a></li>
                    <li><a href="/our-work/around-the-world/guatemala">Guatemela</a></li>
                    <li><a href="/our-work/around-the-world/haiti">Haiti</a></li>
                    <li><a href="/our-work/around-the-world/honduras">Honduras</a></li>
                    <li><a href="/our-work/around-the-world/kenya">Kenya</a></li>
                    <li><a href="/our-work/around-the-world/malawi">Malawi</a></li>
                    <li><a href="/our-work/around-the-world/nicaragua">Nicaragua</a></li>
                    <li><a href="/our-work/around-the-world/philippines">Philippines</a></li>
                    <li><a href="/our-work/around-the-world/tanzania">Tanzania</a></li>
                    <li><a href="/our-work/around-the-world/uganda">Uganda</a></li>
                </ul>
                <a href="/our-work/disaster-response/">Disaster Response</a><br>
                <a href="/our-work/operating-model/">Operating Model</a><br>
                <a href="/our-work/stories/">Stories</a><br>
        </div>
        <div class="col-md-2">
        <h4><a href="/get-involved">Get Involved</a></h4>
                <a href="/get-involved/donate/">Donate</a><br>
                <a href="/get-involved/sponsor-a-child/">Sponsor a Child</a><br>
                <a href="/get-involved/sponsor-a-truck/">Sponsor a Truck</a><br>
                <a href="/get-involved/corporate-partners/">Corporate Partnership</a><br>
                <a href="/get-involved/planned-giving/">Planned Giving</a><br>
                <a href="/get-involved/social-sharing/">Social Sharing</a><br>
                <a href="/get-involved/volunteer/">Volunteer</a><br>
                <a href="/get-involved/advocate/">Advocate</a><br>
                <a href="/get-involved/other-ways-to-give/">Other Ways to Give</a><br>

        </div>
        <div class="col-md-2">
        <h4><a href="/donate/">Donate</a></h4>
        </div>

        <div class="col-md-2">
        <h4><a href="/my-account/">My Account</a></h4>
                <a href="/my-account/">My Account</a><br>
                <a href="/my-account/personal-information">Personal Information</a><br>
                <a href="/my-account/transaction-history">Transaction History</a><br>
                <a href="/my-account/billing-information">Billing Information</a><br>
                <a href="/my-account/my-sponsorships">My Sponsorships</a><br>
                <a href="/my-account/my-monthly-gifts">My Monthly Gifts</a><br>
                <a href="/">Logout</a><br>
        </div>
    </div>

<br>

        <div class="row">
            <div class="col-xs-12">
                <h1>Footer Links Site Map</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
            <h4>Resources</h4>
                <a href="/get-involved/corporate-partners/">Corporate Partners</a><br>
                <a href="/careers/">Careers</a><br>
                <a href="/media-kit/">Media Kit</a><br>
                <a href="/fundraising-toolkit/">Fundraising Toolkit</a><br>
                <a href="/partner-application/">Agency Partner Application</a><br>
                <a href="/teacher-store/">Teacher Store</a><br>
                <a href="/privacy-policy/">Privacy Policy</a><br>
                <a href="/sitemap">Site Map</a><br>
            </div>
            <div class="col-md-4">
            <h4>What You Can Do</h4>
                <a href="/create-your-campaign/">Create Your Campaign</a><br>
                <a href="/get-involved/volunteer/">Volunteer</a><br>
                <a href="/get-involved/other-ways-to-give/#stock">Stock Gifts</a><br>
                <a href="/get-involved/other-ways-to-give/#church">Engage Your Church</a><br>
                <a href="/get-involved/other-ways-to-give/#workplace">Workplace Giving</a><br>
                <a href="/get-involved/other-ways-to-give/#planned">Planned Giving</a><br>

            </div>
            <div class="col-md-4">
            <h4><a href="/contact-us">Contact Us</a></h4>
            </div>
        </div>


    </div>
    <?php include( '../footer.php' ); ?>
    
<script type="text/javascript">
$(document).ready(function(){
  $('body').addClass('blank');
});
</script>
