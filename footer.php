<footer>
	<div class="sponsor-main">
		<div class="row flex-wrap">
			<section class="sponsor-stewards col-12 col-lg-4"> <!--begin stewardship-->
				<div class="recent-sponsors-inner col-12 pl-0 pr-0">
					<div class="col-12 col-sm-9 m-auto pt-4">
						<img src="/images/pie-white.png" alt="93% Program Services" class="img-fluid">
					</div>
					<div class="our-sponsor-header col-12 pr-0 pl-0 mt-4">
						<h2>Good Stewardship</h2>
					</div>
				</div>
			</section> <!--end stewardship-->

			<section class="our-sponsors col-12 col-lg-8 d-flex flex-column"> <!--begin stewardship-->
				<div class="images row align-items-center justify-content-center flex-wrap">

						<div class="col-12 col-lg-4 mt-5 mt-lg-0 text-center">
							<img class="img-fluid" src="/images/footer/Pepsico-logo.png"
							     alt="First slide">
						</div>
						<div class="col-12 col-lg-4 mt-5 mt-lg-0 text-center">
							<img class="img-fluid" src="/images/footer/PriceRite-logo.png"
							     alt="Second slide">
						</div>
						<div class="col-12 col-lg-4 mt-5 mt-lg-0 pr-lg-5 text-center">
							<img class="img-fluid" src="/images/footer/Starkist-logo.png"
							     alt="Third slide">
						</div>

				</div>
				<div class="row our-sponsor-header mt-5 mt-lg-0">
					<div class="col-12">
						<h2 class="text-center">Our Sponsors</h2>
					</div>
				</div>
			</section> <!--end stewardship-->
		</div>
	</div>
	<div class="container footer-container">

		<div class="row">

			<div class="col-xs-12 col-md-4">
				<h2>Resources</h2>
				<ul class="bottom-menu-list">
					<li><a class="bottom-menu-link" href="/get-involved/corporate-partners/">Corporate Partners</a>
					</li>
					<li><a class="bottom-menu-link" href="/careers/">Careers</a></li>
					<li><a class="bottom-menu-link" href="/media-kit/">Media Kit</a></li>
					<li><a class="bottom-menu-link" href="/fundraising-toolkit/">Fundraising Toolkit</a></li>
					<li><a class="bottom-menu-link" href="/partner-application/">Agency Partner Application</a></li>
					<li><a class="bottom-menu-link" href="/teacher-store/">Teacher Store</a></li>
					<li><a class="bottom-menu-link" href="/privacy-policy/">Privacy Policy</a></li>
					<!-- <li><a class="bottom-menu-link" href="#">Vehicle Donation</a></li> -->
					<li><a class="bottom-menu-link" href="/sitemap">Site Map</a></li>
					<li><a class="bottom-menu-link" href="/styles">Style Guide</a></li>  <!-- REMOVE THIS LATER -->


				</ul>
			</div>
			<!-- =================== MIDDLE =================== -->

			<div class="col-xs-12 col-md-4">

				<h2>What You Can Do</h2>
				<ul class="bottom-menu-list">
					<li><a class="bottom-menu-link" href="/create-your-campaign/">Create Your
							Campaign</a></li>
					<li><a class="bottom-menu-link" href="/get-involved/volunteer/">Volunteer</a></li>
					<li><a class="bottom-menu-link" href="/get-involved/other-ways-to-give/#stock">Stock Gifts</a>
					<li><a class="bottom-menu-link" href="/get-involved/other-ways-to-give/#church">Engage Your
							Church</a>
					<li><a class="bottom-menu-link" href="/get-involved/other-ways-to-give/#workplace">Workplace
							Giving</a>
					<li><a class="bottom-menu-link" href="/get-involved/other-ways-to-give/#planned">Planned Giving</a>


				</ul>

			</div>

			<!-- =================== RIGHT =================== -->

			<div class="col-xs-12 col-md-4 newslttr-form">
				<h2>Get Our Newsletter</h2>
				<form class="form-inline">
					<div class="form-group">
						<input type="email" class="form-control" id="inputEmail" placeholder="Email">
					</div>
					<button type="submit" class="btn btn-primary btn-featured">Sign Up</button>
				</form>

				<div class="contact">
					<h2><a href="/contact-us">Contact Us</a></h2>

					<a class="bottom-menu-social-link" href="https://www.facebook.com/feedthechildren">
						<div class="bottom-menu-social-icon">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</div>
					</a>

					<a class="bottom-menu-social-link" href="https://twitter.com/feedthechildren">
						<div class="bottom-menu-social-icon">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</div>
					</a>

					<a class="bottom-menu-social-link" href="https://www.instagram.com/feedthechildrenorg/">
						<div class="bottom-menu-social-icon">
							<i class="fa fa-instagram" aria-hidden="true"></i>
						</div>
					</a>
					<br>
					<a class="bottom-menu-social-link" href="http://youtube.com/FeedTheChildrenOrg">
						<div class="bottom-menu-social-icon">
							<i class="fa fa-youtube-play" aria-hidden="true"></i>
						</div>
					</a>

					<a class="bottom-menu-social-link" href="https://www.pinterest.com/feedthechildren/">
						<div class="bottom-menu-social-icon">
							<i class="fa fa-pinterest-p" aria-hidden="true"></i>
						</div>
					</a>

					<a class="bottom-menu-social-link" href="https://plus.google.com/117160814285599295912/posts">
						<div class="bottom-menu-social-icon">
							<i class="fa fa-google-plus" aria-hidden="true"></i>
						</div>
					</a>
				</div>

			</div>

		</div>
	</div>
</footer>
<div class="botFooter text-center">
	<div class="container">
			<div class="d-flex justify-content-center">
				<div class="copywrite">© <?php echo date("Y"); ?> Feed the Children</div>
				<div>
					<ul>
						<li><a href="/privacy-policy">Privacy Policy</a></li>
						<li><a href="/terms-of-use">Terms of Use</a></li>
						<li><a href="/state-funding">State Fundraising Disclosure Statements</a></li>
					</ul>
				</div>
			</div>
			<p>Feed the Children is a 501(c)(3) non-profit organization. Donations and contributions are tax-deductible as allowed by law.</p>
		</div>
</div>
</body>
</html>
