  <?php include( '../../head.php' ); ?>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h1>Stories</h1>
        <ul>
          <li><a href="/our-work/stories/building-stronger-kids-with-better-food/">Building Stronger Kids with Better Food</a></li>
        </ul>
      </div>
    </div>
  </div>
  <?php include( '../../footer.php' ); ?>
  <!-- This is to style the header for blank pages ( _global.scss ).  -->
<script type="text/javascript">
$(document).ready(function(){
  $('body').addClass('blank');
});
</script>

