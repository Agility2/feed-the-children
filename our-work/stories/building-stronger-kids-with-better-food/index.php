<?php include( '../../../head.php' ); ?>
<div class="interior-hero stories-hero d-flex align-items-center"> <!--HERO-->
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-7">
        <h2>BUILDING STRONGER KIDS WITH BETTER FOOD</h2>
      </div>
    </div>
  </div>
</div> <!--end hero-->
<article class="blog-story">
  <div class="container">

  <div class="post-info row justify-content-lg-center">
    <div class="col-md-12 col-lg-8 d-flex align-items-center flex-wrap">
      <span class="post-date">October 16, 2017</span>
      <div class="post-social">
        <img src="/images/icons/facebook.png" alt="">
        <img src="/images/icons/twitter.png" alt="">
        <img src="/images/icons/google-plus.png" alt="">
      </div>
    </div>
  </div>
  <div class="post-featured-img row justify-content-lg-center">
    <div class="col-md-12 col-lg-8">
      <img src="/images/our-work/stories/featured-image.jpg" alt="">
    </div>
  </div>
  <div class="post-caption row justify-content-lg-center">
    <div class="col-md-12 col-lg-8">
      <h3>Doña María Petzey got up very early one morning to travel from her village in San Antonio Chacayà, Guatemala, to Solala where she would learn how to feed her children and the children of her community.</h3>
      <hr />
    </div>
  </div>
  <div class="post-content row justify-content-lg-center">
    <div class="col-md-12 col-lg-8">
      <p>It seems pretty obvious how to feed kids, but there's a lot more to it than just filling their bellies. So along with other women in her village, Doña María learned 7 different recipes using VitaMeal, fortified rice specially produced by Nu Skin for malnourished kids. VitaMeal is "more than a meal." It does more than feed hungry bellies. VitaMeal builds strong bodies for lifelong health.</p>
      <p>Kids in villages like Doña María's that struggle with poverty suffer from malnutrition because the food they eat is missing vitamins and nutrients that keep their bodies healthy. Their bones aren't strong, and their immune systems are weak. With the help of Nu Skin, Feed the Children uses VitaMeal to help replace what's missing in kids' diets.</p>
      <p>Nu Skin donates an average of 100,000 to 120,000 bags of VitaMeal every month to support Feed the Children programs and partners. And through their Nourish the CHildren (NTC) Initiative, Nu Skin helps fight malnutrition. They work with experts who specialize in better nutrition for countries in the Global South, to make VitaMeal exactly what kids need. It builds bones, maintains muscle funtion and promotes brain development. And healthier food means healthier kids and healthier communities.</p>
      <p>Doña María learned how to feed kids better in her community-- an opportunity she wouldn't have had without Nu Skin's donations of VitaMeal to Feed the Children. As Steve Lund, chairman of Nu Skin Enterprises' board of directors and executive director of the Nourish the Children initiative, recognizes, Nu Skin is "a part of something so much bigger than ourselves."</p>
    </div>
  </div>
  <div class="post-next-prev row justify-content-lg-center">
    <div class="col-md-12 col-lg-8">
      <div class="post-prev">
        <a href="#"><span><i class="fa fa-arrow-left" aria-hidden="true"></i>Previous Post</span></a>
      </div>
      <div class="post-next">
        <a href="#"><span>Next Post <i class="fa fa-arrow-right" aria-hidden="true"></i></span></a>
      </div>
    </div>
  </div>

  </div>
</article>
<section class="more-posts">
  <div class="container">
    <div class="row justify-content-lg-center">
      <div class="col-md-12 col-lg-8">
        <h3>More Inspiring Posts</h3>
      </div>
    </div>
    <div class="row justify-content-lg-center">
      <div class="col-md-12 col-lg-8">
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 more-posts-item">
            <a href="#">
              <img src="/images/our-work/stories/fpo-01.jpg" alt="">
              <span>World Food Day: Kenya's Stance on Food Security</span>
              </a>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 more-posts-item">
            <a href="#">
              <img src="/images/our-work/stories/fpo-02.jpg" alt="">
              <span>World Food Day: Uganda's Stance on Food Security</span>
              </a>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 more-posts-item">
            <a href="#">
              <img src="/images/our-work/stories/fpo-03.jpg" alt="">
              <span>Child Hunger in America: Anna's Story</span>
              </a>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 more-posts-item">
            <a href="#">
              <img src="/images/our-work/stories/fpo-04.jpg" alt="">
              <span>Workplace Giving Recognition: UnitedHealth Group</span>
              </a>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 more-posts-item">
            <a href="#">
              <img src="/images/our-work/stories/fpo-05.jpg" alt="">
              <span>School Breakfast Program Gives Kids a Healthy Start to their Days</span>
              </a>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 more-posts-item">
            <a href="#">
              <img src="/images/our-work/stories/fpo-06.jpg" alt="">
              <span>Kenyan Election Tests Country's New Constitution and Our Operations</span>
              </a>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<?php include( '../../../footer.php' ); ?>
