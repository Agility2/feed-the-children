<?php include( '../head.php' ); ?>
<div class="interior-hero d-flex align-items-end our-work-hero"> <!--HERO-->
<div class="container text-center text-lg-left">
<div class="row">
	<div class="col-12 col-md-12 col-lg-6">
		<h2>Our Work</h2>
	</div>
</div>
</div>
	</div>
	<!-- <nav class="nav nav-tabs" id="myTab" role="tablist">
		<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#in-the-us" role="tab"
		   aria-controls="in-the-us" aria-expanded="true">In the US</a>
		<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#around-world" role="tab"
		   aria-controls="around-world">Around the World</a>
	</nav> -->
</div> <!--end hero-->

<section class="our-work-intro"> <!--our work intro-->
	
<section class="action-map-section"> <!--begin map-->

		<div class="container-fluid"> <!--begin container-->

			<div class="row"> <!--begin row-->


				<!-- =================== LEFT =================== -->

				<div
					class="action-map-left-container col-12 col-sm-12 col-md-12 col-lg-5 col-xl-4 pl-lg-4 pl-xl-5 pr-lg-0">

					<div class="action-map-left pl-lg-4 pl-xl-5">

						<h2>FEED in Action</h2>


						<p class="pr-lg-5">Hunger is a global problem and Feed the Children operates in areas where we
							can make the biggest impact. Use our interactive map to learn more about our work.</p>


						<div class="action-map-areas">

							<label class="btn action-map-button active" data-id="north_america">
								In the USA
								<input value="" name="action-map-area">
							</label>

							<label class="btn action-map-button" data-id="central_america">
								Central America &amp; Caribbean
								<input value="" name="action-map-area">
							</label>

							<label class="btn action-map-button" data-id="south_america">
								South America
								<input value="" name="action-map-area">
							</label>

							<label class="btn action-map-button" data-id="europe">
								Europe
								<input value="" name="action-map-area">
							</label>

							<label class="btn action-map-button" data-id="asia">
								Asia
								<input value="" name="action-map-area">
							</label>

							<label class="btn action-map-button" data-id="africa">
								Africa
								<input value="" name="action-map-area">
							</label>

						</div>


					</div>

				</div>

				<!-- =================== MIDDLE =================== -->

				<div class="action-map-middle-container col-12 col-sm-12 col-md-12 col-lg-7 col-xl-8 p-0">

					<div class="action-map-middle">
						<div class="action-map-middle-map">

							<div class="action-map-google active" id="north_america_id"></div>
							<div class="action-map-google" id="central_america_id"></div>
							<div class="action-map-google" id="south_america_id"></div>
							<div class="action-map-google" id="europe_id"></div>
							<div class="action-map-google" id="asia_id"></div>
							<div class="action-map-google" id="africa_id"></div>

						</div>

					</div>

				</div>

			</div> <!--end row-->

		</div> <!--end container-->

	</section> <!--end action map-->

</section> <!--end work intro-->

<section class="reaching-families">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-sm-12 col-md-6 order-md-2">
				<h2>Reaching Families in Need Across America</h2>
				<p>In the land of plenty, millions of boys and girls are going to bed hungry. More than 17
					million
					households face not having enough food for everyone in the family. Together, we can reach
					families
					in need across America with food and other essentials that offer hope for a better
					future. </p>
			</div>
			<div class="col-sm-12 col-md-6 order-md-1">
				<div class="img-inner">
					<img class="img-fluid" src="/images/our-work/family-photo.png">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="work-change">
<div class="container">
	<div class="TWLA-services-section-2">
		<div class="container-fluid TWLA-full-width">

			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="hunger-main-title">
						<h2 class="text-center mb-3">How Change Happens</h2>
					</div>

				</div>
			</div>

			<div class="row TWLA-row">
				<div class="TWLA-services col-lg-12 col-md-12 col-sm-12 col-12">

					<div class="TWLA-unit col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-2 mb-lg-0">
						<div class="TWLA-cube">
							<div class="TWLA-unit-wrap-default">
								<div class="hunger-image"><img class="text-center img-fluid"
								                               src="/images/our-work/food-programs-icon.png">

								</div>
								<div class="hunger-caption">
									<div class="hunger-head">Domestic Food Programs</div>

									<span class="TWLA-gradient"></span>
								</div>
							</div>
							<div class="TWLA-unit-wrap-hover">
								<div class="hunger-side">
									<h5>Domestic Food Programs</h5>
									<p>Our network of partners across the country means Feed the Children is
										ready to respond
										quickly when tragedy strikes</p>
									<a class="btn" href="/our-work/hunger-usa/">Read More</a>
								</div>
								<span class="TWLA-gradient"></span>
							</div>
						</div>
					</div>

					<div class="TWLA-unit col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-2 mb-lg-0">
						<div class="TWLA-cube">
							<div class="TWLA-unit-wrap-default">
								<div class="hunger-image"><img class="text-center img-fluid"
								                               src="/images/our-work/education-program-icon.png">

								</div>
								<div class="hunger-caption">
									<div class="hunger-head">Education U.S.A</div>

									<span class="TWLA-gradient"></span>
								</div>
							</div>
							<div class="TWLA-unit-wrap-hover">
								<div class="hunger-side">
									<h5>Education U.S.A</h5>
									<p>Our network of partners across the country means Feed the Children is
										ready to respond
										quickly when tragedy strikes</p>
									<a class="btn" href="/our-work/hunger-usa/">Read More</a>
								</div>
								<span class="TWLA-gradient"></span>
							</div>
						</div>
					</div>

					<div class="TWLA-unit col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-2 mb-lg-0">
						<div class="TWLA-cube">
							<div class="TWLA-unit-wrap-default">
								<div class="hunger-image"><img class="text-center img-fluid"
								                               src="/images/our-work/essentials-icon.png">

								</div>
								<div class="hunger-caption">
									<div class="hunger-head">Essentials</div>

									<span class="TWLA-gradient"></span>
								</div>
							</div>
							<div class="TWLA-unit-wrap-hover">
								<div class="hunger-side">
									<h5>Essentials</h5>
									<p>Struggling families need more than food. That's why Feed the Children
										always provides
										household essentials along with food.</p>
									<a class="btn" href="/our-work/hunger-usa/">Read More</a>
								</div>
								<span class="TWLA-gradient"></span>
							</div>
						</div>
					</div>

					<div class="TWLA-unit col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-2 mb-lg-0">
						<div class="TWLA-cube">
							<div class="TWLA-unit-wrap-default">
								<div class="hunger-image"><img class="text-center img-fluid"
								                               src="/images/our-work/disaster-relief-icon.png">

								</div>
								<div class="hunger-caption">
									<div class="hunger-head">Domestic Disaster Relief</div>

									<span class="TWLA-gradient"></span>
								</div>
							</div>
							<div class="TWLA-unit-wrap-hover">
								<div class="hunger-side">
									<h5>Domestic Disaster Relief</h5>
									<p>Our network of partners across the country means Feed the Children is ready
										to respond
										quickly when tragedy strikes.</p>
									<a class="btn" href="/our-work/disaster-response/">Read more</a>
								</div>
								<span class="TWLA-gradient"></span>
							</div>
						</div>
					</div>


				</div>

			</div>
		</div>
	</div>
</div>
</section>

<section class="work-partnership"> <!--our work intro-->
	<div class="container">
		<div class="row justify-content-center align-items-center">
			<div class="col-md-6">
				<h2>The Power of Partnership</h2>
				<p>We believe in working together to end childhood hunger. That's why Feed the Children works with a
					network of thousands of partner agencies in communities across the country. Our partner agencies
					include food pantries, shelters, soup kitchens and churches. These agencies make sure the donated
					food and other essentials reach families in need in the U.S. - free of charge.</p>
				<p><a class="font-weight-bold" href="/get-involved/corporate-partners/">Partner with us today</a></p>
			</div>
			<div class="col-md-6">
				<img class="img-fluid" src="/images/our-work/power-of-partnership.jpg">
			</div>
		</div>
	</div>
</section>

<section class="stories"> <!--our work intro-->
	<div class="container">
		<div class="row">
			<div class="col-md-6">
			</div>
			<div class="col-md-6">
				<h2>Stories</h2>
				<p>Friends like you make a difference for boys and girls in America and around the world. Read these
					amazing stories to discover how lives change when struggling families receive food, essentials, and
					hope.</p>
				<a class="btn btn-primary" href="/our-work/stories/" role="button">Read Stories</a>

			</div>
		</div>
	</div>
</section>

<script>

	(function ($) {
		$('#nav-home-tab').click(function () {
			$('.action-map-middle').addClass('active');
		});
	})(jQuery);
</script>
<?php include( '../footer.php' ); ?>
