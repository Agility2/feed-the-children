<?php include( '../../head.php' ); ?>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1>Around the World</h1>
        <ul>
          <li><a href="/our-work/around-the-world/el-salvador">El Salvador</a></li>
          <li><a href="/our-work/around-the-world/guatemala">Guatemela</a></li>
          <li><a href="/our-work/around-the-world/haiti">Haiti</a></li>
          <li><a href="/our-work/around-the-world/honduras">Honduras</a></li>
          <li><a href="/our-work/around-the-world/kenya">Kenya</a></li>
          <li><a href="/our-work/around-the-world/malawi">Malawi</a></li>
          <li><a href="/our-work/around-the-world/nicaragua">Nicaragua</a></li>
          <li><a href="/our-work/around-the-world/philippines">Philippines</a></li>
          <li><a href="/our-work/around-the-world/tanzania">Tanzania</a></li>
          <li><a href="/our-work/around-the-world/uganda">Uganda</a></li>
        </ul>
      </div>
    </div>
  </div>
<?php include( '../../footer.php' ); ?>

<!-- This is to style the header for blank pages ( _global.scss ).  -->
<script type="text/javascript">
$(document).ready(function(){
  $('body').addClass('blank');
});
</script>
