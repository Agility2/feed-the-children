var action_map_google_maps_icon_directory = "../images/action-map/";

// =============================================================================================================

//  STEP ONE: DECLARE THE UNIQUE SETTINGS FOR EACH OF THE MAPS

// =============================================================================================================

var north_america_map = {
    lat: 39.8283,
    lng: -98.5795, // GEO-CENTER OF US
    html_id: "north_america_id",
    minZoom: 4,
    maxZoom: 6,
    zoomControl: true
};

var central_america_map = {
    lat: 17.0,
    lng: -82.0,
    minZoom: 4,
    maxZoom: 6,
    html_id: "central_america_id"
};

var south_america_map = {
    lat: -15.0,
    lng: -56.0,
    minZoom: 3,
    maxZoom: 6,
    html_id: "south_america_id"
};

var europe_map = {
    lat: 53.0,
    lng: 23.0,
    minZoom: 3,
    maxZoom: 6,
    html_id: "europe_id"
};

var asia_map = {
    lat: 23.0,
    lng: 87.0,
    minZoom: 1,
    maxZoom: 4,
    html_id: "asia_id"
};

var africa_map = {
    lat: 2.0,
    lng: 16.0,
    minZoom: 3,
    maxZoom: 6,
    html_id: "africa_id"
};

// =============================================================================================================

//  STEP TWO: DECLARE THE SETTINGS FOR EACH OF THE PINS THAT WILL APPEAR ON THE MAPS

// =============================================================================================================

var north_america_markers = [];
var central_america_markers = [];
var south_america_markers = [];
var europe_markers = [];
var asia_markers = [];
var africa_markers = [];

central_america_markers.push({
    lat: 13.7942,
    lng: -88.8965,
    markerImage: "orange",
    title: "El Salvador",
    popupInfo: am_text("<a href='/our-work/around-the-world/el-salvador'>El Salvador</a>", "<img style='float:left; padding-right:10px; padding-top:5px;' src='"+ action_map_google_maps_icon_directory + "el-salvador.jpg' /> Since 1987, Feed the Children has worked in El Salvador<br> to create communities where no child goes to bed hungry.")
});

central_america_markers.push({
    lat: 15.7835,
    lng: -90.2308,
    markerImage: "orange",
    title: "Guatemala",
    popupInfo: am_text("<a href='/our-work/around-the-world/guatemala'>Guatemala</a>", "Since 1994, we’ve helped kids in Guatemala<br>defeat hunger and create brighter futures!")
});

central_america_markers.push({
    lat: 18.9712,
    lng: -72.2852,
    markerImage: "orange",
    title: "Haiti",
    popupInfo: am_text("<a href='/our-work/around-the-world/haiti'>Haiti</a>", "Feed the Children serves in the West region of<br>Haiti, helping children and families rebuild<br>their lives after the earthquake.")
});

central_america_markers.push({
    lat: 15.2000,
    lng: -86.2419,
    markerImage: "orange",
    title: "Honduras",
    popupInfo: am_text("<a href='/our-work/around-the-world/Honduras'>Honduras</a>", "In Honduras, Feed the Children works to stop<br>the cycle of poverty for more than 1,300 families<br>in 10 communities each day.")
});

central_america_markers.push({
    lat: 12.8654,
    lng: -85.2072,
    markerImage: "orange",
    title: "Nicaragua",
    popupInfo: am_text("<a href='/our-work/around-the-world/nicaragua'>Nicaragua</a>", "Feed the Children has worked in Nicaragua for<br>22 years and is currently active in 12 different<br>communities around the country.")
});

africa_markers.push({
    lat: -0.0236,
    lng: 37.9062,
    markerImage: "orange",
    title: "Kenya",
    popupInfo: am_text("<a href='/our-work/around-the-world/kenya'>Kenya</a>", "Since 1993, Feed the Children has fought hunger in<br>Kenya, taken in babies who’ve been abandoned and<br>provided care for children with special needs.")
});

africa_markers.push({
    lat: -13.2543,
    lng: 34.3015,
    markerImage: "orange",
    title: "Malawi",
    popupInfo: am_text("<a href='/our-work/around-the-world/malawi'>Malawi</a>", "In over 800 communities throughout Malawi, we work<br>hard to make sure no child goes to bed hungry.")
});

africa_markers.push({
    lat: -6.3690,
    lng: 34.8888,
    markerImage: "orange",
    title: "Tanzania",
    popupInfo: am_text("<a href='/our-work/around-the-world/tanzania'>Tanzania</a>", "More than 35,000 children in Tanzania are empowered<br>to defeat hunger because of Feed the Children.")
});

africa_markers.push({
    lat: 1.3733,
    lng: 32.2903,
    markerImage: "orange",
    title: "Uganda",
    popupInfo: am_text("<a href='/our-work/around-the-world/uganda'>Uganda</a>", "<img style='float:left; padding-right:10px; padding-top:5px;' src='"+ action_map_google_maps_icon_directory + "uganda.jpg' />Feed the Children helps children in Northern<br>Uganda find hope for their future.")
});

asia_markers.push({
    lat: 12.8797,
    lng: 121.7740,
    markerImage: "orange",
    title: "Philippines",
    popupInfo: am_text("<a href='/our-work/around-the-world/philippines'>Philippines</a>", "<img style='float:left; padding-right:10px; padding-top:5px;' src='"+ action_map_google_maps_icon_directory + "philippines.jpg' /> Since 1984 our work expands beyond meals<br>to developing community self-reliance.")
});


/*US markers*/

north_america_markers.push({
    lat: 34.034524,
    lng: -117.59544670000002,
    markerImage: "orange",
    title: "Feed the Children California",
    popupInfo: am_text("<a href='/our-work/hunger-usa/'>Feed the Children California</a>", "Distribution Center & Teacher Store<br/>2551 E. Philadelphia Ave. | Ontario, CA 91761<br/>909-930-5200 x4601 | <a href='mailto:VolunteerCA@feedthechildren.org'>VolunteerCA@feedthechildren.org</a>")
});


north_america_markers.push({
    lat: 35.9943275,
    lng: -86.58663910000001,
    markerImage: "orange",
    title: "La Vergne",
    popupInfo: am_text("<a href='/our-work/hunger-usa/'>La Vergne</a>", "")
});

north_america_markers.push({
    lat: 35.9943275,
    lng: -86.58663910000001,
    markerImage: "orange",
    title: "Feed the Children Tennessee",
    popupInfo: am_text("<a href='/our-work/hunger-usa/'>Feed the Children Tennessee</a>", "Feed the Children Tennessee<br/>310 Tech Park Dr. | La Vergne, TN 37086<br/>615-793-3438 x4401 | <a href='mailto:VolunteerTN@feedthechildren.org'>VolunteerTN@feedthechildren.org</a>")
});

north_america_markers.push({
    lat: 41.723597,
    lng: -85.89590799999996,
    markerImage: "orange",
    title: "Feed the Children Indiana",
    popupInfo: am_text("<a href='/our-work/hunger-usa/'>Feed the Children Indiana</a>", "Distribution Center & Teacher Store<br/>22365 Elkhart East Blvd. | Elkhart, IN 46514<br/>574-262-5227 x4720| <a href='mailto:VolunteerIN@feedthechildren.org'>VolunteerIN@feedthechildren.org</a>")
});

north_america_markers.push({
    lat: 40.67244830000001,
    lng: -75.37740959999996,
    markerImage: "orange",
    title: "Feed the Children Pennsylvania",
    popupInfo: am_text("<a href='/our-work/hunger-usa/'>Feed the Children Pennsylvania</a>", "Distribution Center & Teacher Store<br/>47 S. Commerce Way | Bethlehem, PA 18017<br/>610-419-9356 x4503 | <a href='mailto:VolunteerPA@feedthechildren.org'>VolunteerPA@feedthechildren.org</a>")
});


north_america_markers.push({
    lat: 35.4675562,
    lng: -97.61629149999999,
    markerImage: "orange",
    title: "Distribution Center",
    popupInfo: am_text("<a href='/our-work/hunger-usa/'>Distribution Center</a>", "29 N. McCormick Ave. | Oklahoma City, OK 73127</br>405-949-5168 | Volunteer Services Center – <a href='mailto:VolunteerOK@feedthechildren.org'>VolunteerOK@feedthechildren.org</a><br/>405-945-4018 | Distribution Center")
});

north_america_markers.push({
    lat: 35.137879,
    lng:  -96.61377,
    markerImage: "orange",
    title: "Feed the Children Oklahoma",
    popupInfo: am_text("<a href='/our-work/hunger-usa/'>Feed the Children Oklahoma</a>", "Corporate Offices & Teacher Store<br/>333 N. Meridian Ave.| Oklahoma City, OK 73107<br/>405-945-4029 | <a href='mailto:TeacherStore@feedthechildren.org'>TeacherStore@feedthechildren.org</a>")
});


// =============================================================================================================

//  STEP THREE: DECLARE THE FUNCTION WHICH FORMATS THE POPUP WINDOW TEXT ON THE PINS ABOVE

// =============================================================================================================


function am_text(cityname, text) {

    var html =
        '<div class="action-map-popup-head">' + cityname + '</div>' +
        '<div class="action-map-popup-text">' + text + '</div>';
    return html;
}

// =============================================================================================================

//  HTML/JS MAPPING: THE "all_google_maps" OBJECT IS USED TO ATTACH SCREEN BUTTONS TO GOOGLE MAP DATA

// =============================================================================================================

var all_google_maps = {
    north_america: north_america_map,
    central_america: central_america_map,
    south_america: south_america_map,
    europe: europe_map,
    asia: asia_map,
    africa: africa_map
};

// =============================================================================================================

//  RESOURCE: DEFINE THE GOOGLE MAP PIN IMAGE FILES AND GIVE THEM NICE COLOR NAMES

// =============================================================================================================

var markerImages = {
    magenta: {
        url: action_map_google_maps_icon_directory + "FTC-Map-Marker.png",
        scaledSize: new google.maps.Size(40, 40)
    },
    orange: {
        url: action_map_google_maps_icon_directory + "FTC-Map-Marker.png",
        scaledSize: new google.maps.Size(40, 40)
    },
    green: {
        url: action_map_google_maps_icon_directory + "FTC-Map-Marker.png",
        scaledSize: new google.maps.Size(40, 40)
    },
    blue: {
        url: action_map_google_maps_icon_directory + "FTC-Map-Marker.png",
        scaledSize: new google.maps.Size(40, 40)
    },
    uganda: {
        url: action_map_google_maps_icon_directory + "uganda.jpg",
        scaledSize: new google.maps.Size(40, 40)
    }
};


// =============================================================================================================

//  RESOURCE: SOME OF THE SETTINGS TO CUSTOMIZE GOOGLE MAPS - HERE ARE ONES CHANGED AND ONES THAT COULD BE CHANGED

// =============================================================================================================

var map_style = [{
    elementType: 'geometry',
    stylers: [{
        color: '#ffffff'
    }]
}, {
    elementType: 'labels.text.stroke',
    stylers: [{
        color: '#ffffff'
    }]
}, {
    elementType: 'labels.text.fill',
    stylers: [{
        color: '#000'
    }]
}, {
    featureType: 'administrative.country',
    elementType: 'geometry.text.fill',
    stylers: [{
        color: '#999999'
    }]
}, {
    featureType: 'administrative.country',
    elementType: 'labels.text',
    stylers: [{
        visibility: 'off'
    }]
}, {
    featureType: 'administrative.country',
    elementType: 'labels.text.fill',
    stylers: [{
        visibility: 'off'
    }]
}, {
    featureType: 'administrative.province',
    elementType: 'geometry.text.fill',
    stylers: [{
        color: '#000000'
    }]
}, {
    featureType: 'administrative.province',
    elementType: 'labels.text',
    stylers: [{
        visibility: 'off'
    }]
}, {
    featureType: 'administrative.province',
    elementType: 'labels.text.fill',
    stylers: [{
        visibility: 'off'
    }]
}, {
    featureType: 'administrative.locality',
    elementType: 'labels.text.fill',
    stylers: [{
        "color": "#CFCFCF"
    }]
}, {
    featureType: 'water',
    elementType: 'geometry',
    stylers: [{
        color: '#6082b6'
    }]
}, {
    featureType: 'water',
    elementType: 'labels.text',
    stylers: [{
        visibility: 'off'
    }]
}, {
    featureType: 'water',
    elementType: 'labels.text.fill',
    stylers: [{
        visibility: 'off'
    }]
}, {
    featureType: 'water',
    elementType: 'labels.text.stroke',
    stylers: [{
        visibility: 'off'
    }]
}];

// =============================================================================================================

//  RESOURCE: GOOGLE MAP DISPLAY AND CONTROL SETTINGS - A DEFAULT SET THAT CAN BE OVERRIDDEN AT A MAP LEVEL

// =============================================================================================================

var map_default_settings = {
    styles: map_style
};

var marker_default_settings = {
    // STANDARD SETTINGS ACROSS ALL MARKERS GO HERE
};


// =============================================================================================================

//  LOGIC: THESE FUNCTIONS APPLY DEFAULT AND CUSTOM SETTINGS TO GOOGLE MAP ELEMENTS

// =============================================================================================================

function am_get_map_settings(mapInfo) {
    // THIS FUNCTION TAKES A DEFAULT SET OF SETTINGS FOR ALL MAPS, ADDS THAT
    // TO THE SETTINGS DEFINED IN A GIVEN MAP, MAKES SOME ADJUSTMENTS (ZOOM),
    // CALCULATES SOME PARMS (LATLNG FROM LAT & LNG) AND DELETES OUT ALL NON
    // RELEVANT PARMS WHICH ARE NOT SETTINGS BEFORE RETURNING THE SET TO CREATE
    // A MAP

    var settings = $.extend({}, map_default_settings, mapInfo);

    if ("zoom" in mapInfo) {
        settings.zoom = mapInfo.zoom;
        settings.minZoom = mapInfo.zoom;
        settings.maxZoom = mapInfo.zoom;
    }

    settings.center = new google.maps.LatLng(mapInfo.lat, mapInfo.lng);

    // DELETE PROPERTIES THAT ARE NOT SETTINGS BUT WERE INCLUDED IN $.extend()
    delete settings.lat;
    delete settings.lng;
    delete settings.html_id;
    return settings;
}

function am_get_marker_settings(mapInfo, markerInfo) {
    // THIS FUNCTION CREATES THE SETTINGS FOR EACH MARKER. IT TAKES
    // A DEFAULT SET OF SETTINGS, ADDS THE MARKER SPECIFIC SETTINGS AND
    // USES THE "markerImage" PARM (IF PRESENT) TO GRAB A URL AND SCALEDSIZE
    // FROM THE markerImages ARRAY. IT THEN CALCULATES SOME SETTING
    // FIELDS AND DELETES SOME NON-SETTINGS FIELDS BEFORE RETURNING THE RESULT.

    var settings = $.extend({}, marker_default_settings, markerInfo);

    settings.map = mapInfo.mapObj; // link the marker to a map

    if ("markerImage" in markerInfo && markerInfo["markerImage"] in markerImages) {
        settings.icon = markerImages[markerInfo["markerImage"]];
    } else {
        settings.icon = markerImages["black"];
    }

    settings.position = new google.maps.LatLng(markerInfo.lat, markerInfo.lng);

    delete settings.lat;
    delete settings.lng;
    delete settings.markerImage;
    delete settings.popupInfo;

    return settings;
}

// =============================================================================================================

//  LOGIC: THESE FUNCTIONS GENERATE THE GOOGLE MAPS

// =============================================================================================================


// ON LOAD - INITIALIZE ALL THE MAPS
google.maps.event.addDomListener(window, 'load', function () {
    am_init_all_maps();
});
// variable to hold current active InfoWindow
var activeInfoWindow ;	

function am_init_all_maps() {

    // PROGRAMMING NOTE: THIS CODE HIDES THE BUTTONS FOR THE MAPS NOT BEING GENERATED

    $(document).ready(function () {
        $('.action-map-button[data-id="south_america"]').hide();
        $('.action-map-button[data-id="europe"]').hide();

        // PROGRAMMING NOTE: COMMENT OUT THE MAPS NOT BEING GENERATED

        am_init_a_map(north_america_map, north_america_markers);
        am_init_a_map(central_america_map, central_america_markers);
        // am_init_a_map( south_america_map,   south_america_markers );
        // am_init_a_map( europe_map,          europe_markers );
        am_init_a_map(asia_map, asia_markers);
        am_init_a_map(africa_map, africa_markers);

        // THE FOLLOWING LISTENER WAITS UNTIL THE NORTH AMERICA MAP LOADS THEN REFRESHES
        // IT. THIS IS A HACK TO FORCE RENDERING THE FIRST GOOGLE MAP IMAGE

        google.maps.event.addListenerOnce(north_america_map.mapObj, 'idle', function () {
            action_map_first_time();
        });

    });
}

function am_init_a_map(mapInfo, markersInfo) {
    mapInfo.mapObj = new google.maps.Map(
        document.getElementById(mapInfo.html_id),
        am_get_map_settings(mapInfo)
    );

    am_create_map_event_listeners(mapInfo);

    am_put_markers_on_map(mapInfo, markersInfo);
}


function am_put_markers_on_map(mapInfo, markersInfo) {
    var bounds = new google.maps.LatLngBounds();

    for (var i = 0; i < markersInfo.length; i++) {

        var markerInfo = markersInfo[i];
        var settings = am_get_marker_settings(mapInfo, markerInfo);

        markerInfo.markerObj = new google.maps.Marker(settings);
        bounds.extend(settings.position);

        var message = markerInfo.popupInfo;

        am_attach_popup_message(mapInfo.mapObj, markerInfo.markerObj, message);

    }

    mapInfo.mapObj.fitBounds(bounds);

}

function am_attach_popup_message(map, marker, message) {
    var popupVar = new google.maps.InfoWindow({
        content: message
    });

    marker.addListener('click', function () {
        popupVar.open(marker.get('map'), marker);
    });

// Hover listeners for the google map markers
    google.maps.event.addListener(marker, 'mouseover', function() {

        // Close active window if exists - [one might expect this to be default behaviour no?]				
				if(activeInfoWindow != null) activeInfoWindow.close();

                
        popupVar.open(marker.get('map'), marker);

        // Store new open InfoWindow in global variable
				activeInfoWindow = popupVar;
    });
    google.maps.event.addListener(marker, 'mouseout', function() {
    });
// end hover listeners

    map.addListener('click', function () {
        popupVar.close();
    });


}


function am_create_map_event_listeners(mapInfo) {

    // KEEP THE MAP CENTERED IF THE SCREEN IS RESIZED
    google.maps.event.addDomListener(window, "resize", function () {
        var center = mapInfo.mapObj.getCenter();
        google.maps.event.trigger(mapInfo.mapObj, "resize");
        mapInfo.mapObj.setCenter(center);
    });

}



// =============================================================================================================
// END
// =============================================================================================================
