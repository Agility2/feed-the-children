// Sticky Nav

jQuery(document).ready(function ($) {

    $(window).scroll(function () {
        var sticky = $('.sticky');
        var logoImg = $('.fixed .desktop-logo .logo');
        scroll = $(window).scrollTop();

        if (scroll >= $('.global-header').outerHeight()) {

            sticky.addClass('fixed');
            logoImg.attr("src", "/images/FTC-Logo-Color.png");

        } else {

            sticky.removeClass('fixed');
            logoImg.attr("src", "/images/FTC-Logo-White.png");
        }
    });
});


// HACK TO MAKE THE HELP-NOW COLUMNS THE SAME SIZE
// CALLED ON LOAD AND IF SOMEONE CHANGES THE COLUMN SIZE

function same_height() {
    var nbr = 0;
    if (nbr < $(".help-now-left").height()) nbr = $(".help-now-left").height();
    if (nbr < $(".help-now-right").height()) nbr = $(".help-now-right").height();
    $(".help-now-left").height(nbr);
    $(".help-now-left-middle-1").height(nbr);
    $(".help-now-left-middle-2").height(nbr);
    $(".help-now-right").height(nbr);
}

$(window).resize(function () {
    same_height();
});

$(document).ready(function (e) {
    same_height();
});


// HANDLE THE CLICKING OF THE HELP NOW DONATION BUTTONS AND OTHER-AMOUNT FIELD

function help_now_donate_clicked(id) {
    $("#donate25").removeClass("active");
    $("#donate25 input").removeClass("active");
    $("#donate50").removeClass("active");
    $("#donate50 input").removeClass("active");
    $("#donate100").removeClass("active");
    $("#donate100 input").removeClass("active");
    $("#donate250").removeClass("active");
    $("#donate250 input").removeClass("active");


    if (id == "help-now-other-amount") {
        var temp = 0;
        // FUTURE PROCESS ON AMOUNT - VERIFY FORMAT, ETC
    } else {
        $(id).addClass("active");
        $(id + " input").addClass("active");
        $("#help-now-other-amount").val("");
    }

}

$(document).ready(function (e) {
    $("#donate25").click(function (e) {
        help_now_donate_clicked("donate25");
    });
    $("#donate50").click(function (e) {
        help_now_donate_clicked("donate50");
    });
    $("#donate100").click(function (e) {
        help_now_donate_clicked("donate100");
    });
    $("#donate250").click(function (e) {
        help_now_donate_clicked("donate250");
    });
});


function action_map_first_time() {
    // THIS FUNCTION MAKES SURE THE FIRST BUTTON AND FIRST MAP ARE "ACTIVE"
    $(".action-map-button.active").removeClass("active");
    $(".action-map-button:first-of-type").addClass("active");

    $(".action-map-google.active").removeClass("active");
    $(".action-map-google:first-of-type").addClass("active");

    id = $(".action-map-google:first-of-type").prop("id").slice(0, -3);

    var actual_mapInfo = all_google_maps[id];
    action_map_map_refresh(actual_mapInfo);
}


// PROCESS THE USER CLICKING ON ANY OF THE AREA BUTTONS

function action_map_area_clicked(t) {
    var google_map_name = $(t).attr("data-id");
    var google_map_id = "#" + google_map_name + "_id";

    // CHANGE THE STATE OF THE BUTTONS (active/inactive)

    $(".action-map-button.active").removeClass("active");
    $(t).addClass("active");

    // CHANGE THE DISPLAY OF THE MAP DIVs (display/hidden)

    $(".action-map-google.active").removeClass("active");
    $(google_map_id).addClass("active");

    // ASK GOOGLE TO 'RESIZE' THE MAP

    var actual_mapInfo = all_google_maps[google_map_name];
    action_map_map_refresh(actual_mapInfo);
}


function action_map_map_refresh(mapInfo) {
    var mapObj = mapInfo.mapObj;
    var c = mapObj.getCenter();
    google.maps.event.trigger(mapObj, 'resize');

    var latlng = {
        lat: mapInfo.lat,
        lng: mapInfo.lng
    };

    mapObj.setCenter(latlng);
}

// ACTIVE THE CLICKING ON THE LEFT SIDE AREA BUTTONS
// NOTE THAT WE ACTIVATE BOTH THE CLASS (WHICH IS ATTACHED TO THE LABEL TAG)
// AND THEN THE SUBORDINATE INPUT TAG
$(document).ready(function (e) {

    $(".action-map-button").on("click", function () {
        action_map_area_clicked(this);
        return false;
    });

    $(".action-map-button input").on("click", function () {
        action_map_areas_clicked(this);
        return false;
    });

});

// activate tooltips
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

//sticky header
$(window).scroll(function () {
    var logo = $('.top-navbar-header');
    var menu = $('.top-navbar .top-navbar-menu');
    var logoImg = $('#logo');
    scroll = $(window).scrollTop();

    if (scroll >= 60) {
        menu.addClass('fixedHeader');
        logo.addClass('fixedLogo');
        logoImg.attr("src", "./images/aspot/FTC-Logo-Color.png");
    } else {
        menu.removeClass('fixedHeader');
        logo.removeClass('fixedLogo');
        logoImg.attr("src", "./images/aspot/FTC-Logo-White.png");

    }
});

//enable search window
$(document).ready(function (e) {
    var searchBtn = $('#searchBtn');
    var formWrapper = $('.form-wrapper');
    var closeSearch = $('.close-search');
    searchBtn.on("click", function () {
        formWrapper.css({display: "block"});
    });

    closeSearch.on("click", function () {
        formWrapper.css({display: "none"});
    });

});

//validate numbers only
function validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}


// =============================================================================================================
// Homepage Help Now Section - Fields appear when checkbox is clicked.
// =============================================================================================================

$(document).ready(function () {
    $('.help-now-dedicate-button #styled-checkbox-1').change(function () {
        $('#help-now-dedicate-fields').toggleClass('hidden');
    });
});


// =============================================================================================================
// Youtube Lazyload function
// =============================================================================================================
$(document).ready(function () {
    (function () {

        var youtube = document.querySelectorAll(".youtube");

        for (var i = 0; i < youtube.length; i++) {

            if (typeof youtube[i].dataset.cover !== "undefined") {
                var source = youtube[i].dataset.cover;
            } else {
                var source = "https://img.youtube.com/vi/" + youtube[i].dataset.embed + "/sddefault.jpg";
            }


            var image = new Image();
            image.src = source;
            image.addEventListener("load", function () {
                youtube[i].appendChild(image);
            }(i));

            youtube[i].addEventListener("click", function () {

                var iframe = document.createElement("iframe");

                iframe.setAttribute("frameborder", "0");
                iframe.setAttribute("allowfullscreen", "");
                iframe.setAttribute("src", "https://www.youtube.com/embed/" + this.dataset.embed + "?rel=0&showinfo=0&&controls=0&autoplay=1&frameborder=0");

                this.innerHTML = "";
                this.appendChild(iframe);
            });
        }
        ;

    })();
});

// =============================================================================================================
// Slider
// =============================================================================================================
$(document).ready(function () {
    $('.carousel').carousel();
});

// =============================================================================================================
// Homepage sponsor child
// =============================================================================================================
(function ($) {

    /*show form function*/
    function showForm() {
        $('.show-form').click(function () {
            var hidden = $('.form-slide');
            var shrinkSlide = $('.shrink-slide');

            if (hidden.hasClass('not-visible')) {
                shrinkSlide.animate({"right": "0px"}, "slow");
                hidden.animate({"right": "-1000px"}, "slow").removeClass('not-visible');
            } else {

                shrinkSlide.animate({"right": "180px"}, "slow");

                hidden.animate({"right": "0px"}, "slow").addClass('not-visible');
            }

        });
    }

    /*Activating before 992px width*/
    $(document).ready(function () {
        if ($(window).width() > 992) {
            showForm();
        }
    });
})(jQuery);