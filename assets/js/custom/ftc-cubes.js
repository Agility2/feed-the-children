
var ease = {
    line: "linear",
    In: "ease-in",
    Out: "cubic-bezier(0.215, 0.610, 0.355, 1.000)",
    OutCirc: "cubic-bezier(0.075, 0.820, 0.165, 1.000)",
    InOut: "ease-in-out",
    InOutBack: "cubic-bezier(0.680, -0.550, 0.265, 1.550);"
};

function ianIt(selector, css) {
    var _css = css;
    var selectorArr = [];
    if ((selector != null) && (selector.length > 0) && (typeof selector !== 'string')) selectorArr = selector;
    else selectorArr.push(selector);

    for (var i = 0; i < selectorArr.length; i++)
        $(selectorArr[i]).css(_css);
}

var anItQueue = [], anItQueueSelectors = [];
var QueueIsWork=false;
function _anIt(selector, css, time, delay, easing, _callback) {
    anItQueue.push({
        selector: selector,
        css: css,
        time: time,
        delay: delay,
        easing: easing,
        _callback: _callback
    });

    if (!QueueIsWork) applyAnItQueue();
}

function applyAnItQueue(){
    QueueIsWork=true;
    requestAnimFrame(function () {

        var qItem = anItQueue.shift();
        _anIt(qItem.selector, qItem.css, qItem.time, qItem.delay, qItem.easing, qItem._callback);

        if (anItQueue.length!==0) applyAnItQueue();
        else QueueIsWork=false;

    });
}
function anIt(selector, css, time, delay, easing, _callback) {

    var applyTransitionTime=0,
        selectorArr = [],selectorArrLen,
        clearLastCallback = (typeof css.noClearLastCallback !== 'undefined') ? !css.noClearLastCallback : true,
        callback = (typeof _callback !== 'undefined') ? _callback : function () {},
        prevTimeOutId=null,
        i=0;

    if ((selector != null) && (selector.length > 0) && (typeof selector !== 'string')) selectorArr = selector;
    else selectorArr.push(selector);

    if (Modernizr.csstransitions) {
        var _css = css,_cssTransition={},_css_arr = Object.keys(css),_css_arr_len=_css_arr.length;
        _cssTransition["transition-duration"] = time + 'ms';
        _cssTransition["transition-timing-function"] = easing;

        _cssTransition["transition-property"] = '';

        for(i=0;i<_css_arr_len;i++){
            if (_css_arr[i]!=='noClearLastCallback') _cssTransition["transition-property"]+=_css_arr[i];
            if (i<(_css_arr_len-1)) _cssTransition["transition-property"]+=', ';
        }

        var to = setTimeout(function () {
            selectorArrLen=selectorArr.length;
            var cto = setTimeout(function () {
                _cssTransition = {};
                _cssTransition["transition-duration"] = '';
                _cssTransition["transition-timing-function"] = '';
                _cssTransition["transition-property"] = '';
                for (i = 0; i < selectorArrLen; i++)
                    $(selectorArr[i]).css(_cssTransition);

                callback();

            }, time + applyTransitionTime);

            for (i = 0; i < selectorArrLen; i++) {
                var $item = $(selectorArr[i]);

                if (clearLastCallback) {
                    prevTimeOutId = $item.attr('data-aito');
                    clearTimeout(prevTimeOutId);
                }

                $item.css(_cssTransition).attr({'data-aito': cto});

                setTimeout(function(){
                    $item.css(_css);
                },applyTransitionTime);
            }

        }, delay);

    } else {
        selectorArrLen=selectorArr.length;
        for (var i = 0; i < selectorArrLen; i++)
            $(selectorArr[i]).css(css);

        callback();
    }
}
window.requestAnimFrame =
    window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        };

var sitePreloadData = [];

var loaderProgressViewsObjs = {};

var loaderAnimTime = 300;
var toRenderP = 0;
var isRenderEnd=false;

function startPreload() {

    loaderProgressViewsObjs.$top = $("#preloader .top");
    loaderProgressViewsObjs.$left = $("#preloader .left");
    loaderProgressViewsObjs.$bottom = $("#preloader .bottom");
    loaderProgressViewsObjs.$right = $("#preloader .right");

    $('img').each(function () {
        sitePreloadData.push($(this).attr('src'));
    });

    load_res_arr(sitePreloadData, function (p) {
        toRenderP = p;
    }, function () {
        toRenderP=1;
    });

   anIt(loaderProgressViewsObjs.$top,{'width':'100%'},loaderAnimTime,0,ease.line,function(){
       anIt(loaderProgressViewsObjs.$right,{'height':'100%'},loaderAnimTime,0,ease.line,function(){
           anIt(loaderProgressViewsObjs.$bottom,{'width':'100%'},loaderAnimTime,0,ease.line,function(){
               anIt(loaderProgressViewsObjs.$left,{'height':'100%'},loaderAnimTime,0,ease.line,function(){
                   anIt('#preloader', {'opacity': 0}, 500, 0, ease.Out, function () {
                       $('#preloader').remove();
                   });
               })
           })
       })
   })
}
(function addXhrProgressEvent(cash) {
    var originalXhr = $.ajaxSettings.xhr;
    $.ajaxSetup({
        progress: function () {
        },
        xhr: function () {
            var req = originalXhr(), that = this;
            if (req) {
                if (typeof req.addEventListener == "function") {
                    req.addEventListener("progress", function (evt) {
                        var percentLoaded;
                        if (evt.lengthComputable) percentLoaded = parseInt((evt.loaded / evt.total), 10);
                        that.progress(evt, percentLoaded, that.url);
                    }, false);
                }
            }
            return req;
        }
    });
})(jQuery);

function load_res_arr(arr, percents_fn, callback) {


    var totalSizeInBytes = 0;
    var progressBar = [];
    var arrLen = arr.length;
    // callback()
    var headProgress = 0;
    var headProgressCoef = 0.1;
    load_by_ajax('HEAD', arr, function () {
    }, HEAD_success, function () {
        load_by_ajax('GET', arr, progressAll, function () {
        }, callback);
    });

    var headSuccessCount = 0;

    function HEAD_success(data, text, resp, path) {
        progressBar.push({file_l: 0, file_n: path});
        totalSizeInBytes += parseInt(resp.getResponseHeader('Content-Length'));

        headProgress = (headSuccessCount / arrLen) * headProgressCoef;
        percents_fn(headProgress);
        headSuccessCount++;
    }

    function progressAll(evt, percent, url) {
        var totalLoadedInBytes = 0
        for (var i = 0; i < arrLen; i++) {
            if (progressBar[i].file_n == url) {
                progressBar[i].file_l = evt.loaded;
            }
            totalLoadedInBytes += progressBar[i].file_l
        }

        var p = (totalLoadedInBytes / totalSizeInBytes) * (1 - headProgressCoef);
        percents_fn(headProgress + p);
    }
}

function load_by_ajax(type, arr, progress, success, callback) {

    var filesTotal = arr.length;
    var fileLoaded = 0;
    var errors = 0;
    var limit = 8;
    var next_limit;

    next_limit = fileLoaded + limit;
    load_limit(fileLoaded, fileLoaded + limit);


    function load_limit(s, end) {
        var _end = Math.min(filesTotal, end);

        for (var i = s; i < _end; i++) {
            ResLoad(i);
        }
    }

    function ResLoad(fileToLoad) {
        var path;
        path = arr[fileToLoad];
        $.ajax({
            type: type,
            url: path,
            cache: true,
            beforeSend: function (xhr) {
                xhr.overrideMimeType("text/plain; charset=x-user-defined");
            },
            progress: function (evt, percent, url) {
                progress(evt, percent, url)
            },
            success: function (data, text, resp) {
                success(data, text, resp, path);
                onLoad();
            },
            error: function () {
                onError(fileToLoad)
            }
        });

        function onLoad() {
            fileLoaded++;

            if (fileLoaded == filesTotal) callback();
            if (fileLoaded == next_limit) {
                next_limit = fileLoaded + limit;
                if (fileLoaded < filesTotal) load_limit(fileLoaded, fileLoaded + limit);
            }
        }

        function onError(fileToLoad) {
            errors++;
            if (errors < 10) {
                ResLoad(fileToLoad)
            }
            else {
                fileLoaded++;
                if (fileLoaded == filesTotal) callback();
                if (fileLoaded == next_limit) {
                    next_limit = fileLoaded + limit;
                    if (fileLoaded < filesTotal) load_limit(fileLoaded, fileLoaded + limit);
                }
            }
        }
    }
}

;(function($){
    $(window).bind("resize", function(){
      onResize();
    });
    onResize();
}(jQuery));


function onResize(){
    resize();
    scalePopup();
}


function scalePopup(){
    var WW = $(window).width() ? $(window).width() : window.screen.availWidth;
    var coef = WW / 1280;

    if($(window).height()>690){
        $("#projectsPopup .container, #projectsPopup").removeAttr("style");
    }else{
        $("#projectsPopup .container").removeAttr("style");
        var newHeight = ($("#projectsPopup").height() / coef) * 0.9;
        var oldHeight = $("#projectsPopup .container").height();

        $("#projectsPopup .container").css({"transform":"scale("+(newHeight/oldHeight)+")"});
    }

}

function resize(){

    var WW = $(window).width() ? $(window).width() : window.screen.availWidth;
    var coef = WW / 1280;

    $("#spincube, #projectsPopup").css({"transform":"scale("+coef+")"});

    if($("#clone").length){
        $("#clone").css({"transform":"scale("+coef+")"});
    }
   // $("#services .icon img, #contacts .ico img").css({"transform":"scale("+(1280/WW)+")"});
}


$(document).ready( function(e) {
    var offsetTop = 0;
    $('.TWLA-services-section-2 .TWLA-unit').hover(function(){
       cubeSlider(1,['X','Y'],$(this).find('.TWLA-unit-wrap-hover')[0],$(this).find('.TWLA-unit-wrap-default')[0],$(this).find('.TWLA-cube')[0], $.noop);
       },function(){
       cubeSlider(-1,['X','Y'],$(this).find('.TWLA-unit-wrap-default')[0],$(this).find('.TWLA-unit-wrap-hover')[0],$(this).find('.TWLA-cube')[0],function(){
           ianIt($(this).find('.TWLA-unit-wrap-default')[0],{'transform':''});
       });
   });
});

var animBaseTime=700;

function cubeSlider(_dir,orientation,next,now,cont,cb){
   var OriginNow, OriginNext, dir, deg_dir, perspective=1000;

   if(orientation[0]=='X'){
       dir=_dir;
       deg_dir=_dir;
       OriginNow  = (dir==1) ? '100% 50%' : '0% 50%';
       OriginNext = (dir==1) ? '0% 50%'   : '100% 50%';
   } else if (orientation[0]=='Y'){
       dir=_dir;
       deg_dir=(-1)*_dir;
       OriginNow  = (dir==1) ? '50% 100%'   : '50% 0%';
       OriginNext = (dir==1) ? '50% 0%' : '50% 100%';
   }

   ianIt(next,{'transform':'translate'+orientation[0]+'('+dir*100+'%) rotate'+orientation[1]+'('+deg_dir*90+'deg)','transform-origin':OriginNext});
   ianIt(now,{'transform-origin':OriginNow});
   ianIt(cont,{'perspective':perspective+'px','transform-style': 'preserve-3d','z-index':500});

   setTimeout(function(){
       anIt(cont,{'transform':'scale(0.9)'},animBaseTime/5,0,ease.Out,function(){
           anIt(cont,{'transform':'scale(1)'},animBaseTime-animBaseTime/5,0,ease.Out,function(){
           });
       });

       anIt(next,{'transform':'translate'+orientation[0]+'(0%) rotate'+orientation[1]+'(0deg)'},animBaseTime,0,ease.Out,function(){
       });

       $(now).find('.TWLA-gradient').css({'visibility':'visible'});
       anIt($(now).find('.TWLA-gradient')[0],{'opacity':1},animBaseTime,0,ease.Out);
       anIt($(next).find('.TWLA-gradient')[0],{'opacity':0},animBaseTime,0,ease.Out,function(){
           $(next).find('.TWLA-gradient').css({'visibility':'hidden'});
       });

       anIt(now,{'transform':'translate'+orientation[0]+'('+(-1*dir*100)+'%) rotate'+orientation[1]+'('+(-1*deg_dir*90)+'deg)'},animBaseTime,0,ease.Out,function(){
           cb();
           ianIt(cont,{'perspective':perspective+'px','transform-style': 'preserve-3d','z-index':1000});
       });
   },0);
}

