<?php include( '../../head.php' ); ?>

<!-- START OF STYLES FOR HEADER IN MY ACCOUNT ONLY! -->
<style>
.global-header {
    background:#666;
    position:relative;
}
@media (max-width: 1199px) {
    .global-header {
    background:#fff;
    border-bottom:1px solid #eee;
}
}
li.nav-item.dropdown:last-child > a { 
    color:#ff9a33;
}
</style>
<!-- END OF STYLES FOR HEADER IN MY ACCOUNT ONLY! -->

<div class="container myAccount">
<div class="row">
    <div class="col-md-3">
    <ul id="myAccountMenu">
    <li><a href="/my-account/">My Account</a></li>
    <li><a href="/my-account/personal-information">Personal Information</a></li>
    <li class="maActive"><a href="/my-account/transaction-history">Transcation History</a></li>
    <li><a href="/my-account/billing-information">Billing Information</a></li>
    <li><a href="/my-account/my-sponsorships">My Sponsorships</a></li>
    <li><a href="/my-account/my-monthly-gifts">My Monthly Gifts</a></li>
    </ul>
    </div>

    <div class="col-md-9">
            <div id="maPage">
                <div class="userBar d-flex justify-content-between">
                    <div class="userName align-self-center">Hi, Anthony Matiya</div>
                    <div class="logOut align-self-center"><a href="/">Log Out</a></div>
                </div> <!-- end of userBar -->

                <div id="transactionHistory" class="maContainer">
                    <div class="maHeader">
                    <h2>Transaction History </h2>
                    <h3>Recent Transactions <span>(All classes and donations are tax free)</span></h3>                
                        <hr>
                    </div> <!-- end of maHeader -->
                    <div class="maContent">
                        <form id="displayTrans">
                        <div class="form-group row">
                                <label for="display" class="col-sm-1 col-form-label disLabel">Display:</label>
                                <div class="col-sm-4">     
                                    <select id="inputDisplay" class="form-control">
                                    <option selected>Recent Orders</option>
                                        <option>...</option>
                                    </select>
                                    <button type="submit" class="btn btn-go">Go</button>
                                </div>
                            </div>
                        </form>

                        <div class="transTable">
                                <hr>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-3">
                                            Type:<br>
                                            <strong>Monthly Gifts</strong>
                                        </div>
                                        <div class="col-md-3">
                                            Date:<br>
                                            <strong>October 06, 2017</strong>
                                        </div>
                                        <div class="col-md-4">
                                            Receipt:<br>
                                            <a href="#"><img src="../../images/my-account/35x35-Printer-Icon.jpg">   Printable Tax Receipt</a>
                                        </div>
                                        <div class="col-md-2">
                                            Total: <br>
                                            <strong>$25.00</strong><br>
                                            <a href="#" class="totalDetail">View Details</a>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-3">
                                            Type:<br>
                                            <strong>Child Sponsorship</strong>
                                        </div>
                                        <div class="col-md-3">
                                            Date:<br>
                                            <strong>September 22, 2017</strong>
                                        </div>
                                        <div class="col-md-4">
                                            Receipt:<br>
                                            <a href="#"><img src="../../images/my-account/35x35-Printer-Icon.jpg">   Printable Tax Receipt</a>
                                        </div>
                                        <div class="col-md-2">
                                            Total: <br>
                                            <strong>$34.00</strong><br>
                                            <a href="#" class="totalDetail">View Details</a>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-3">
                                            Type:<br>
                                            <strong>One Time Gifts</strong>
                                        </div>
                                        <div class="col-md-3">
                                            Date:<br>
                                            <strong>June 06, 2017</strong>
                                        </div>
                                        <div class="col-md-4">
                                            Receipt:<br>
                                            <a href="#"><img src="../../images/my-account/35x35-Printer-Icon.jpg">   Printable Tax Receipt</a>
                                        </div>
                                        <div class="col-md-2">
                                            Total: <br>
                                            <strong>$250.00</strong><br>
                                            <a href="#" class="totalDetail">View Details</a>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                        </div>
                    </div> <!-- end of maContent -->
                </div> <!-- end of maContainer -->
            </div> <!-- end of maPage -->
        </div> <!-- end of col-md-9 -->
        
    </div> <!-- end of row -->
</div> <!-- end of container -->


<?php include( '../footer.php' ); ?>
</body>
</html>