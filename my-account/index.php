<?php include( '../head.php' ); ?>

<!-- START OF STYLES FOR HEADER IN MY ACCOUNT ONLY! -->
<style>
.global-header {
    background:#666;
    position:relative;
}
@media (max-width: 1199px) {
    .global-header {
    background:#fff;
    border-bottom:1px solid #eee;
}
}

li.nav-item.dropdown:last-child > a { 
    color:#ff9a33;
}
</style>
<!-- END OF STYLES FOR HEADER IN MY ACCOUNT ONLY! -->


<div class="container myAccount">
    <div class="row">
        <div class="col-md-3">
            <ul id="myAccountMenu">
                <li class="maActive"><a href="#">My Account</a></li>
                <li><a href="/my-account/personal-information">Personal Information</a></li>
                <li><a href="/my-account/transaction-history">Transcation History</a></li>
                <li><a href="/my-account/billing-information">Billing Information</a></li>
                <li><a href="/my-account/my-sponsorships">My Sponsorships</a></li>
                <li><a href="/my-account/my-monthly-gifts">My Monthly Gifts</a></li>
            </ul>
        </div> <!-- end of col-md-3 -->
        <div class="col-md-9">
            <div id="maPage">
                <div class="userBar d-flex justify-content-between">
                    <div class="userName align-self-center">Hi, Anthony Matiya</div>
                    <div class="logOut align-self-center"><a href="/">Log Out</a></div>
                </div> <!-- end of userBar -->
                <div class="warningMessage">
                    <img src="../../images/my-account/92x93-Alert-Icon.png" />
                    <div class="wmContent">
                        <h5><strong>Please update your billing information.</strong></h5>
                        <p>Your credit card on file ending in 2222 will expire on 11/2017. Please <a href="/my-account/billing-information">update</a> your payment information.  </p>
                    </div> <!-- end of wmContent -->
                    <div class="wmClose">
                        <img src="../../images/my-account/Close-Window-Icon.png" />
                    </div> <!-- end of wmClose -->
                </div><!-- end of warningMessage  -->

                <div id="myAccount" class="maContainer">
                    <div class="maHeader">
                        <h2>Account Summary</h2>
                        <h3>My Donations</h3>
                        <hr>
                    </div> <!-- end of maHeader -->

                    <div class="maContent">

                        <div class="givingSummary">
                            <h3> <strong>2017 GIVING SUMMARY</strong> </h3>
                        
                            <div class="giveItems d-flex justify-content-between">
                                <div class="giveItemName">One Time Gifts</div><div class="giveItemLine"></div><div class="giveItemPrice">$60.00</div>
                            </div>

                            <div class="giveItems d-flex justify-content-between">
                                <div class="giveItemName">Child Sponsorship</div><div class="giveItemLine"></div><div class="giveItemPrice">$60.00</div>
                            </div>

                            <div class="giveItems d-flex justify-content-between">
                                <div class="giveItemName">Monthly Gifts</div><div class="giveItemLine"></div><div class="giveItemPrice">$200.00</div>
                            </div>

                            <div class="barTotal d-flex justify-content-between">
                                <div class="totalName"> <strong>Total Contribution to Date</strong></div>
                                <div class="totalNumber"><strong>$320.00</strong></div>
                            </div>

                        </div> <!-- end of givingSummary -->

                        <div class="givingSummary">
                            <h3> <strong>2016 GIVING SUMMARY</strong> </h3>
                        
                            <div class="giveItems d-flex justify-content-between">
                                <div class="giveItemName">One Time Gifts</div><div class="giveItemLine"></div><div class="giveItemPrice">$50.00</div>
                            </div>

                            <div class="giveItems d-flex justify-content-between">
                                <div class="giveItemName">Child Sponsorship</div><div class="giveItemLine"></div><div class="giveItemPrice">$320.00</div>
                            </div>

                            <div class="giveItems d-flex justify-content-between">
                                <div class="giveItemName">Monthly Gifts</div><div class="giveItemLine"></div><div class="giveItemPrice">$1,200.00</div>
                            </div>

                            <div class="barTotal d-flex justify-content-between">
                                <div class="totalName"> <strong>Total Contribution to Date</strong></div>
                                <div class="totalNumber"><strong>$1,570.00</strong></div>
                            </div>
                        </div> <!-- end of givingSummary -->
                        
                        <div id="bannerAds">
                             <div class="bannerAd giveBackpacks">
                                <div class="bannerAdContent">
                                     <h3>GIVE BACKPACKS FULL OF HOPE</h3>
                                     <a href="/get-involved/other-ways-to-give/" class="adBtn">I WANT TO GIVE!</a>
                                 </div>
                           </div>
    
                            <div class="bannerAd giftWings">
                                 <div class="bannerAdContent">
                                      <h3>YOUR GIFT HAS WINGS</h3>
                                      <a href=" /donate/" class="adBtn">GIVE A GIFT!</a>
                                </div>
                            </div>
                        </div><!-- end of adBanners -->

                    </div> <!-- end of maContent -->
                </div> <!-- end of maContainer -->
            </div> <!-- end of maPage -->
        </div> <!-- end of col-md-9 -->
    </div> <!-- end of row -->
</div> <!-- end of container -->
<script>
$('.wmClose').click(function(){
    $(".warningMessage").hide();
});
</script>
 <?php include( 'footer.php' ); ?>
</body>
</html>