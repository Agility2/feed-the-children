<?php include( '../../head.php' ); ?>

<!-- START OF STYLES FOR HEADER IN MY ACCOUNT ONLY! -->
<style>
.global-header {
    background:#666;
    position:relative;
}
@media (max-width: 1199px) {
    .global-header {
    background:#fff;
    border-bottom:1px solid #eee;
}
}
li.nav-item.dropdown:last-child > a {
    color:#ff9a33;
}
</style>
<!-- END OF STYLES FOR HEADER IN MY ACCOUNT ONLY! -->


<div class="container myAccount">
<div class="row">
    <div class="col-md-3">
    <ul id="myAccountMenu">
    <li><a href="/my-account/">My Account</a></li>
    <li><a href="/my-account/personal-information">Personal Information</a></li>
    <li><a href="/my-account/transaction-history">Transcation History</a></li>
    <li><a href="/my-account/billing-information">Billing Information</a></li>
    <li><a href="/my-account/my-sponsorships">My Sponsorships</a></li>
    <li class="maActive"><a href="/my-account/my-monthly-gifts">My Monthly Gifts</a></li>
    </ul>
    </div>



    <div class="col-md-9">
            <div id="maPage">
                <div class="userBar d-flex justify-content-between">
                    <div class="userName align-self-center">Hi, Anthony Matiya</div>
                    <div class="logOut align-self-center"><a href="/">Log Out</a></div>
                </div> <!-- end of userBar -->

                <div id="myMonthlyGifts" class="maContainer">
                    <div class="maHeader">
                    <h2> My Monthy Gifts </h2>
                    <h3><a href="#">View Monthly Statement</a></h3>
                        <hr>
                    </div> <!-- end of maHeader -->
                    <div class="maContent monthGTable">
                    <form id="displayTrans">
                        <div class="form-group row">
                                <label for="display" class="col-sm-1 col-form-label disLabel">Display:</label>
                                <div class="col-sm-4">
                                    <select id="inputDisplay" class="form-control">
                                    <option selected>Date</option>
                                        <option>...</option>
                                    </select>
                                    <button type="submit" class="btn btn-go">Go</button>
                                </div>
                            </div>
                        </form>

                        <hr>

                        <div class="container">
                                <div class="row">
                                    <div class="col-md-3">
                                    Type:<br>
                                            <strong>Monthly Gifts</strong>
                                    </div>
                                    <div class="col-md-2">
                                    Date:<br>
                                            <strong>10-18-2017</strong>
                                    </div>
                                    <div class="col-md-3">
                                    Receipt:<br>
                                    <img class="receiptIcon" src="../../images/my-account/35x35-Printer-Icon.jpg"> <a href="#"> Printable Tax Receipt</a>
                                    </div>
                                    <div class="col-md-2">
                                        Payment: <br>
                                        <strong>Visa Ending in 2222</strong>
                                    </div>
                                    <div class="col-md-2">
                                        Total: <br>
                                        <strong>$25.00</strong><br>
                                            <a href="#">Edit</a>
                                    </div>
                                </div><!-- end row 1 -->


                                <hr>


                                <div class="row">
                                    <div class="col-md-3">
                                    Type:<br>
                                            <strong>Monthly Gifts</strong>
                                    </div>
                                    <div class="col-md-2">
                                    Date:<br>
                                            <strong>10-02-2017</strong>
                                    </div>
                                    <div class="col-md-3">
                                    Receipt:<br>
                                    <img class="receiptIcon" src="../../images/my-account/35x35-Printer-Icon.jpg"> <a href="#"> Printable Tax Receipt</a>
                                    </div>
                                    <div class="col-md-2">
                                        Payment: <br>
                                        <strong>AmEx Ending in 2112</strong>
                                    </div>
                                    <div class="col-md-2">
                                        Total: <br>
                                        <strong>$50.00</strong><br>
                                            <a href="#">Edit</a>
                                    </div>
                                </div><!-- end row 2 -->

                                <hr>

                                <div class="row">
                                    <div class="col-md-3">
                                    Type:<br>
                                            <strong>Special Gift: Backpack Donations</strong>
                                    </div>
                                    <div class="col-md-2">
                                    Date:<br>
                                            <strong>09-22-2017</strong>
                                    </div>
                                    <div class="col-md-3">
                                    Receipt:<br>
                                    <img class="receiptIcon" src="../../images/my-account/35x35-Printer-Icon.jpg"> <a href="#"> Printable Tax Receipt</a>
                                    </div>
                                    <div class="col-md-2">
                                        Payment: <br>
                                        <strong>Visa Ending in 1111</strong>
                                    </div>
                                    <div class="col-md-2">
                                        Total: <br>
                                        <strong>$33.00</strong><br>
                                            <a href="#">Edit</a>
                                    </div>
                                 </div> <!-- end row 3 -->

                                <hr>

                                <div class="row">
                                    <div class="col-md-3">
                                    Type:<br>
                                            <strong>Monthly Gifts</strong>
                                    </div>
                                    <div class="col-md-2">
                                    Date:<br>
                                            <strong>10-18-2017</strong>
                                    </div>
                                    <div class="col-md-3">
                                    Receipt:<br>
                                    <img class="receiptIcon" src="../../images/my-account/35x35-Printer-Icon.jpg"> <a href="#"> Printable Tax Receipt</a>
                                    </div>
                                    <div class="col-md-2">
                                        Payment: <br>
                                        <strong>Visa Ending in 2222</strong>
                                    </div>
                                    <div class="col-md-2">
                                        Total: <br>
                                        <strong>$25.00</strong><br>
                                            <a href="#">Edit</a>
                                    </div>
                                </div><!-- end row 4 -->

                                <hr>

                                <div class="row">
                                    <div class="col-md-3">
                                    Type:<br>
                                            <strong>Monthly Gifts</strong>
                                    </div>
                                    <div class="col-md-2">
                                    Date:<br>
                                            <strong>10-18-2017</strong>
                                    </div>
                                    <div class="col-md-3">
                                    Receipt:<br>
                                    <img class="receiptIcon" src="../../images/my-account/35x35-Printer-Icon.jpg"> <a href="#"> Printable Tax Receipt</a>
                                    </div>
                                    <div class="col-md-2">
                                        Payment: <br>
                                        <strong>Visa Ending in 2222</strong>
                                    </div>
                                    <div class="col-md-2">
                                        Total: <br>
                                        <strong>$25.00</strong><br>
                                            <a href="#">Edit</a>
                                    </div>
                                </div><!-- end row 5 -->
                        </div> <!-- end of container -->

                    <div class="monPagi">
                    <i class="fa fa-chevron-left" aria-hidden="true"></i> <span>1-5 of 35</span> <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </div>
                    <div class="largeBannerAd">
                        <div class="lbaContent d-flex justify-content-between">
                            <div class="lbaText">
                            <h3><strong>Sponsor a Child</strong></h3>
                            <p>Join the fight to end hunger.</p>
                            </div>
                            <div class="lbaButton">
                                <a href="/get-involved/sponsor-a-child/" class="adBtn"> <i class="fa fa-heart" aria-hidden="true"></i> PROCEED</a>
                            </div>
                        </div>
                    </div>
                    </div> <!-- end of maContent -->
                </div> <!-- end of maContainer -->
            </div> <!-- end of maPage -->
        </div> <!-- end of col-md-9 -->

    </div> <!-- end of row -->
</div> <!-- end of container -->
<?php include( '../footer.php' ); ?>
</body>
</html>
