<footer class="myAccountFooter"> <!-- begin footer -->
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="footer-items">
					<div class="footer-logo">
						<img src="/images/donation/footer-logo.png">
					</div>
					<div class="footer-menu">
						<ul>
							<li><a href="/privacy-policy/">Privacy Policy</a></li>
                            <li><a href="/terms-of-use/">Terms of Use</a></li>
							<li><a href="/state-funding/">State Funding</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
