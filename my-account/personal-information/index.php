<?php include( '../../head.php' ); ?>

<!-- START OF STYLES FOR HEADER IN MY ACCOUNT ONLY! -->
<style>
.global-header {
    background:#666;
    position:relative;
}
@media (max-width: 1199px) {
    .global-header {
    background:#fff;
    border-bottom:1px solid #eee;
}
}
li.nav-item.dropdown:last-child > a { 
    color:#ff9a33;
}
</style>
<!-- END OF STYLES FOR HEADER IN MY ACCOUNT ONLY! -->


<div class="container myAccount">
<div class="row">
    <div class="col-md-3">
    <ul id="myAccountMenu">
    <li><a href="/my-account/">My Account</a></li>
    <li class="maActive"><a href="/my-account/personal-information">Personal Information</a></li>
    <li><a href="/my-account/transaction-history">Transcation History</a></li>
    <li><a href="/my-account/billing-information">Billing Information</a></li>
    <li><a href="/my-account/my-sponsorships">My Sponsorships</a></li>
    <li><a href="/my-account/my-monthly-gifts">My Monthly Gifts</a></li>
    </ul>
    </div>

    <div class="col-md-9">
            <div id="maPage">
                <div class="userBar d-flex justify-content-between">
                    <div class="userName align-self-center">Hi, Anthony Matiya</div>
                    <div class="logOut align-self-center"><a href="/">Log Out</a></div>
                </div> <!-- end of userBar -->

                <div id="personalInformation" class="maContainer">
                    <div class="maHeader">
                        <h2> Personal Information </h2>
                        <h3>Account #: 76432105</h3>
                        <hr>
                    </div> <!-- end of maHeader -->
                    <div class="maContent">

                        <form id="PersonalInfo">
                        <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label"><strong>Name:</strong></label>
                                <div class="col-sm-4">     
                                    <input type="text" class="form-control" id="InputFirstName" placeholder="First Name">
                                </div>
                                <div class="col-sm-4">     
                                    <input type="text" class="form-control" id="InputLastName"  placeholder="Last Name">
                                </div>
                            </div>
                            <br>
                            <div class="form-group row">
                                <label for="InputEmail" class="col-sm-3 col-form-label"><strong>Email Address:</strong></label>
                                <div class="col-sm-8">     
                                    <input type="email" class="form-control" id="InputEmail"  placeholder="Enter email">
                                </div>
                            </div>
                            <br>
                            <div class="form-group row">
                                <label for="address" class="col-sm-3 col-form-label"><strong>Address:</strong></label>
                                <div class="col-sm-8">     
                                    <input type="text" class="form-control" id="InputAddress"  placeholder="Address">
                                </div>
                            </div>
                            <br>
                            <div class="form-group row">
                                <label for="address2" class="col-sm-3 col-form-label"><strong>Address 2:</strong></label>
                                <div class="col-sm-8">     
                                    <input type="text" class="form-control" id="InputAddress2"  placeholder="Address 2">
                                </div>
                            </div>
                            <br>
                            <div class="form-group row">
                                <label for="city" class="col-sm-3 col-form-label"><strong>City:</strong></label>
                                <div class="col-sm-8">     
                                    <input type="text" class="form-control" id="InputCity"  placeholder="City">
                                </div>
                            </div>
                            <br>
                            <div class="form-group row">
                                <label for="state" class="col-sm-3 col-form-label"><strong>State:</strong></label>
                                <div class="col-sm-8">     
                                    <select id="inputState" class="form-control">
                                    <option selected>- Select One -</option>
                                    <option value="AL">Alabama</option>
	<option value="AK">Alaska</option>
	<option value="AZ">Arizona</option>
	<option value="AR">Arkansas</option>
	<option value="CA">California</option>
	<option value="CO">Colorado</option>
	<option value="CT">Connecticut</option>
	<option value="DE">Delaware</option>
	<option value="DC">District Of Columbia</option>
	<option value="FL">Florida</option>
	<option value="GA">Georgia</option>
	<option value="HI">Hawaii</option>
	<option value="ID">Idaho</option>
	<option value="IL">Illinois</option>
	<option value="IN">Indiana</option>
	<option value="IA">Iowa</option>
	<option value="KS">Kansas</option>
	<option value="KY">Kentucky</option>
	<option value="LA">Louisiana</option>
	<option value="ME">Maine</option>
	<option value="MD">Maryland</option>
	<option value="MA">Massachusetts</option>
	<option value="MI">Michigan</option>
	<option value="MN">Minnesota</option>
	<option value="MS">Mississippi</option>
	<option value="MO">Missouri</option>
	<option value="MT">Montana</option>
	<option value="NE">Nebraska</option>
	<option value="NV">Nevada</option>
	<option value="NH">New Hampshire</option>
	<option value="NJ">New Jersey</option>
	<option value="NM">New Mexico</option>
	<option value="NY">New York</option>
	<option value="NC">North Carolina</option>
	<option value="ND">North Dakota</option>
	<option value="OH">Ohio</option>
	<option value="OK">Oklahoma</option>
	<option value="OR">Oregon</option>
	<option value="PA">Pennsylvania</option>
	<option value="RI">Rhode Island</option>
	<option value="SC">South Carolina</option>
	<option value="SD">South Dakota</option>
	<option value="TN">Tennessee</option>
	<option value="TX">Texas</option>
	<option value="UT">Utah</option>
	<option value="VT">Vermont</option>
	<option value="VA">Virginia</option>
	<option value="WA">Washington</option>
	<option value="WV">West Virginia</option>
	<option value="WI">Wisconsin</option>
	<option value="WY">Wyoming</option>
                                    </select>
                                </div>
                            </div>
                            <br>

                            <div class="form-group row">
                                <label for="postalcode" class="col-sm-3 col-form-label"><strong>Postal Code:</strong></label>
                                <div class="col-sm-8">     
                                    <input type="text" class="form-control" id="Inputpc"  placeholder="Postal Code">
                                </div>
                            </div>
                            <br>

                            <div class="form-group row">
                                <label for="phoneNumber" class="col-sm-3 col-form-label"><strong>Phone Number:</strong></label>
                                <div class="col-sm-5">     
                                    <input type="text" class="form-control" id="Inputpn"  placeholder="Phone Number">
                                </div>
                                <div class="col-sm-3">     
                                <select id="inputTypePhone" class="form-control">
                                <option selected>Mobile</option>
                                    <option>Home</option>
                                    <option>Office</option>
                                </select>
                              </div>
                            </div>
                            <br>

                            <button type="submit" class="btn btn-primary">UPDATE</button>
                        </form>


                        <form id="resetPass">
                                <h3><strong>Reset Password</strong></h3>
                                <br>
                            <div class="form-group row">
                                <label for="cp" class="col-sm-3 col-form-label"><strong>Current Password:</strong></label>
                                <div class="col-sm-8">     
                                    <input type="password" class="form-control" id="Inputcp"  placeholder="Current Password">
                                </div>
                            </div>
                            <br>
                            <div class="form-group row">
                                <label for="np" class="col-sm-3 col-form-label"><strong>New Password:</strong></label>
                                <div class="col-sm-8">     
                                    <input type="password" class="form-control" id="Inputnp"  placeholder="New Password">
                                </div>
                            </div>
                            <br>
                            <div class="form-group row">
                                <label for="rnp" class="col-sm-3 col-form-label"><strong>Retype New Password:</strong></label>
                                <div class="col-sm-8">     
                                    <input type="password" class="form-control" id="Inputrnp"  placeholder="Retype New Password">
                                </div>
                            </div>
                            <br>

                            <button type="submit" class="btn btn-primary"> UPDATE </button>
                        </form>            
                        
                        
                        <form id="CommsPreferences">
                                <div class="d-flex justify-content-between">
                                <h3><strong>Communication Preferences</strong></h3>
                                <a href="#" style="padding-right:15px;"><strong>Our Privacy Policy</strong></a>
                                </div>
                                <hr>
                                <p><strong>Please select which topics resonate with you from the following list.
We'll send you more information:</strong></p>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                        <input class="form-check-input uncheck" type="checkbox"> FEED The Children in the US
                                        </label>
                                    </div>
                                    </div>
                                </div>
                     


                                <div class="form-group row">
                                    <div class="col-sm-12">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                        <input class="form-check-input uncheck" type="checkbox"> FEED the Children Around the World
                                        </label>
                                    </div>
                                    </div>
                                </div>
                                


                                <div class="form-group row">
                                    <div class="col-sm-12">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                        <input class="form-check-input uncheck" type="checkbox"> Sponsor a Child
                                        </label>
                                    </div>
                                    </div>
                                </div>
                              

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                        <input class="form-check-input uncheck" type="checkbox"> Sponsor a Truck
                                        </label>
                                    </div>
                                    </div>
                                </div>
                               



                                <div class="form-group row">
                                    <div class="col-sm-12">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                        <input class="form-check-input uncheck" type="checkbox"> Disaster Response
                                        </label>
                                    </div>
                                    </div>
                                </div>
                     



                                <div class="form-group row">
                                    <div class="col-sm-12">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                        <input class="form-check-input clearAllChecks" type="checkbox"> Please don't send me email correspondence
                                        </label>
                                    </div>
                                    </div>
                                </div>
                                <br>
                            <button type="submit" class="btn btn-primary"> UPDATE </button>
                        </form>      

                    </div> <!-- end of maContent -->
                </div> <!-- end of maContainer -->
            </div> <!-- end of maPage -->
        </div> <!-- end of col-md-9 -->
        
    </div> <!-- end of row -->
</div> <!-- end of container -->
<script>
    $(".clearAllChecks").click(function() {
        $('.uncheck').prop('checked', false);
    });
    $(".uncheck").click(function() {
        $('.clearAllChecks').prop('checked', false);
    });
</script>

<?php include( '../footer.php' ); ?>
</body>
</html>