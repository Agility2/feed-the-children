<?php include( '../../head.php' ); ?>

<!-- START OF STYLES FOR HEADER IN MY ACCOUNT ONLY! -->
<style>
.global-header {
    background:#666;
    position:relative;
}
@media (max-width: 1199px) {
    .global-header {
    background:#fff;
    border-bottom:1px solid #eee;
}
}
li.nav-item.dropdown:last-child > a { 
    color:#ff9a33;
}
</style>
<!-- END OF STYLES FOR HEADER IN MY ACCOUNT ONLY! -->

<div class="container myAccount">
<div class="row">
    <div class="col-md-3">
    <ul id="myAccountMenu">
    <li><a href="/my-account/">My Account</a></li>
    <li><a href="/my-account/personal-information">Personal Information</a></li>
    <li><a href="/my-account/transaction-history">Transcation History</a></li>
    <li><a href="/my-account/billing-information">Billing Information</a></li>
    <li class="maActive"><a href="/my-account/my-sponsorships">My Sponsorships</a></li>
    <li><a href="/my-account/my-monthly-gifts">My Monthly Gifts</a></li>
    </ul>
    </div>



    
    <div class="col-md-9">
            <div id="maPage">
                <div class="userBar d-flex justify-content-between">
                    <div class="userName align-self-center">Hi, Anthony Matiya</div>
                    <div class="logOut align-self-center"><a href="/">Log Out</a></div>
                </div> <!-- end of userBar -->

                <div id="mySponsorships" class="maContainer">
                    <div class="maHeader">
                    <h2> My Sponsorships </h2>
                    <div class="d-flex justify-content-between">
                    <h3><a href="#">View Monthly Statement</a></h3>
                    <a href="#faq"><strong>FAQ</strong></a>
                    </div>
                      <hr>
                    </div> <!-- end of maHeader -->
                    <div class="maContent sponsorships">

                    <div class="childTable">
                        <hr>

                        <div class="container">


                            <div class="row">
                                <div class="col-md-2">
                                <img src="../../images/my-account/154x154-Hanna-Banda.jpg" />
                                </div>
                                <div class="col-md-2">
                                    ID #: <br> 
                                    <strong>372421</strong>
                                </div>
                                <div class="col-md-2">
                                    Name: <br>
                                    <strong>Hanna Banda</strong>
                                </div>
                                <div class="col-md-2">
                                    Country: <br>
                                    <strong>Kenya</strong>
                                </div>
                                <div class="col-md-2">
                                    Monthly: <br> 
                                    <strong>$34</strong>
                                </div>
                                <div class="col-md-2">
                                        Months Sponsored:  <br>
                                        <strong>2 Months</strong>
                                        <a href="#">Edit</a>
                                </div>
                            </div>
                        <hr>


                    <div class="row">
                            <div class="col-md-2">
                            <img src="../../images/my-account/154x154-Sara-Damaris.jpg" />
                            </div>
                            <div class="col-md-2">
                                ID #: <br> 
                                <strong>169891</strong>
                            </div>
                            <div class="col-md-2">
                                Name: <br>
                                <strong>Sara Damaris Banegas</strong>
                            </div>
                            <div class="col-md-2">
                                Country: <br>
                                <strong>Honduras</strong>
                            </div>
                            <div class="col-md-2">
                                Monthly: <br> 
                                <strong>$34</strong>
                            </div>
                            <div class="col-md-2">
                                    Months Sponsored:  <br>
                                    <strong>3 Months</strong>
                                    <a href="#">Edit</a>
                            </div>
                        </div>
                    <hr>



                    <div class="row">
                            <div class="col-md-2">
                            <img src="../../images/my-account/154x154-Denis-Antonio.jpg" />
                            </div>
                            <div class="col-md-2">
                                ID #: <br> 
                                <strong>247001</strong>
                            </div>
                            <div class="col-md-2">
                                Name: <br>
                                <strong>Denis Antonio Hernandez</strong>
                            </div>
                            <div class="col-md-2">
                                Country: <br>
                                <strong>Ecuador</strong>
                            </div>
                            <div class="col-md-2">
                                Monthly: <br> 
                                <strong>$34</strong>
                            </div>
                            <div class="col-md-2">
                                    Months Sponsored:  <br>
                                    <strong>6 Months</strong>
                                    <a href="#">Edit</a>
                            </div>
                        </div>
                    <hr>



                    <div class="row">
                            <div class="col-md-2">
                            <img src="../../images/my-account/154x154-John-Wesley.jpg" />
                            </div>
                            <div class="col-md-2">
                                ID #: <br> 
                                <strong>145289</strong>
                            </div>
                            <div class="col-md-2">
                                Name: <br>
                                <strong>John Wesley</strong>
                            </div>
                            <div class="col-md-2">
                                Country: <br>
                                <strong>Kenya</strong>
                            </div>
                            <div class="col-md-2">
                                Monthly: <br> 
                                <strong>$34</strong>
                            </div>
                            <div class="col-md-2">
                                    Months Sponsored:  <br>
                                    <strong>12 Months</strong>
                                    <a href="#">Edit</a>
                            </div>
                        </div>
                    <hr>
                    
                    </div>
                    </div>
                    
                    <div class="childColumn">
                        <h2><strong>Do you want to sponsor another child?</strong></h2>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3"><a href="/get-involved/sponsor-a-child/" style="text-align:center; display: block;"><img src="../../images/my-account/300x300-Another-Child-A.jpg">
                   Sponsor Felicia</a>
                            </div>
                                <div class="col-md-3"><a href="/get-involved/sponsor-a-child/" style="text-align:center; display: block;"><img src="../../images/my-account/300x300-Another-Child-B.jpg">
                                Sponsor John</a>
                            </div>
                                <div class="col-md-3"><a href="/get-involved/sponsor-a-child/" style="text-align:center; display: block;"><img src="../../images/my-account/300x300-Another-Child-C.jpg">
                                Sponsor Luke</a>
                            </div>
                                <div class="col-md-3"><a href="/get-involved/sponsor-a-child/" style="text-align:center; display: block;"><img src="../../images/my-account/300x300-Another-Child-D.jpg">
                                Sponsor Lisa</a>
                            </div>
                            </div>
                        </div>
                    </div>
        <br>
                    <div id="faq">
                        <h2><strong>Sponsorship FAQs</strong></h2>

                        <div id="exampleAccordion" data-children=".item">

                        <div class="item">
                            <a class="questionTitle" data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion1" aria-expanded="true" aria-controls="exampleAccordion1">
                            <i class="fa" aria-hidden="true"></i> How long should my child sponsorship last?
                            </a>
                            <div id="exampleAccordion1" class="collapse show" role="tabpanel">
                            <p class="mb-3">
                            Praesent sagittis interdum orci nec finibus. Phasellus pellentesque hendrerit massa a varius.
                            Morbi convallis tincidunt nisl, non efficitur felis dictum id.
                              </p>
                            </div>
                        </div>

   
                        <div class="item">
                            <a class="questionTitle collapsed" data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion2" aria-expanded="false" aria-controls="exampleAccordion2">
                            <i class="fa" aria-hidden="true"></i> How much does it cost to sponsor a child?
                            </a>
                            <div id="exampleAccordion2" class="collapse" role="tabpanel">
                            <p class="mb-3">
                                Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            </p>
                            </div>
                        </div>


         
                        <div class="item">
                            <a class="questionTitle collapsed" data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion3" aria-expanded="false" aria-controls="exampleAccordion3">
                            <i class="fa" aria-hidden="true"></i> What specific benefits will the child I sponsor receive?
                            </a>
                            <div id="exampleAccordion3" class="collapse" role="tabpanel">
                            <p class="mb-3">
                                Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            </p>
                            </div>
                        </div>
             
                        <div class="item">
                            <a class="questionTitle collapsed" data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion4" aria-expanded="false" aria-controls="exampleAccordion4">
                            <i class="fa" aria-hidden="true"></i> How can I communicate with the child I sponsor?
                            </a>
                            <div id="exampleAccordion4" class="collapse" role="tabpanel">
                            <p class="mb-3">
                                Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            </p>
                            </div>
                        </div>                        
                        
                        <div class="item">
                            <a class="questionTitle collapsed" data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion5" aria-expanded="false" aria-controls="exampleAccordion5">
                            <i class="fa" aria-hidden="true"></i> What do I do if I am considering canceling my child sponsorship?
                            </a>
                            <div id="exampleAccordion5" class="collapse" role="tabpanel">
                            <p class="mb-3">
                                Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            </p>
                            </div>
                        </div>      
                        
                        <div class="item">
                            <a class="questionTitle collapsed" data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion6" aria-expanded="false" aria-controls="exampleAccordion6">
                            <i class="fa" aria-hidden="true"></i> Will I be the only person sponsoring the child I'm helping?
                            </a>
                            <div id="exampleAccordion6" class="collapse" role="tabpanel">
                            <p class="mb-3">
                                Donec at ipsum dignissim, rutrum turpis scelerisque, tristique lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus nec dui turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            </p>
                            </div>
                        </div>      
                        
                        </div>
                        <br> 
                    </div>



                    </div> <!-- end of maContent -->
                </div> <!-- end of maContainer -->
            </div> <!-- end of maPage -->
        </div> <!-- end of col-md-9 -->
        
    </div> <!-- end of row -->
</div> <!-- end of container -->

<?php include( '../footer.php' ); ?>
</body>
</html>