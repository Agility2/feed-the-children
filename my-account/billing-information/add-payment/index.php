<?php include( '../../../head.php' ); ?>

<!-- START OF STYLES FOR HEADER IN MY ACCOUNT ONLY! -->
<style>
.global-header {
    background:#666;
    position:relative;
}
@media (max-width: 1199px) {
    .global-header {
    background:#fff;
    border-bottom:1px solid #eee;
}
}
li.nav-item.dropdown:last-child > a { 
    color:#ff9a33;
}
</style>
<!-- END OF STYLES FOR HEADER IN MY ACCOUNT ONLY! -->


<div class="container myAccount">
<div class="row">
    <div class="col-md-3">
    <ul id="myAccountMenu">
    <li><a href="/my-account/">My Account</a></li>
    <li><a href="/my-account/personal-information">Personal Information</a></li>
    <li><a href="/my-account/transaction-history">Transcation History</a></li>
    <li class="maActive"><a href="/my-account/billing-information">Billing Information</a></li>
    <li><a href="/my-account/my-sponsorships">My Sponsorships</a></li>
    <li><a href="/my-account/my-monthly-gifts">My Monthly Gifts</a></li>
    </ul>
    </div>

    <div class="col-md-9">
            <div id="maPage">
                <div class="userBar d-flex justify-content-between">
                    <div class="userName align-self-center">Hi, Anthony Matiya</div>
                    <div class="logOut align-self-center"><a href="/">Log Out</a></div>
                </div> <!-- end of userBar -->

                <div id="billingInformation" class="maContainer">
                    <div class="maHeader">
                    <h2> Add Payment Method </h2>
                        <hr>
                    </div> <!-- end of maHeader -->
                    <div class="maContent">

                        <div class="container">

                                <form>
                                <div class="form-group row">
                                <select id="inputTypePhone" class="form-control col-sm-8">
                                <option selected>Payment Type</option>
                                    <option>Visa</option>
                                    <option>Master Card</option>
                                    <option>American Express</option>
                                    <option>Discovery</option>
                                </select>
                                </div>
                                <br>
                                <div class="form-group row">
                                <input class="form-control col-sm-8" type="text" placeholder="Credit Card Number *">
                                </div>
                                <br>
                                <div class="form-group row">
                                <input class="form-control col-sm-3" type="number" placeholder="CVV *">
                                </div>
                                <br>
                                <div class="form-group row">
                                <select id="inputTypePhone" class="form-control col-sm-4">
                                <option selected>Expire Month *</option>
                                   <option value="1">01 - January</option>
                                    <option value="2">02 - February</option>
                                    <option value="3">03 - March</option>
                                    <option value="4">04 - April</option>
                                    <option value="5">05 - May</option>
                                    <option value="6">06 - June</option>
                                    <option value="7">07 - July</option>
                                    <option value="8">08 - August</option>
                                    <option value="9">09 - September</option>
                                    <option value="10">10 - October</option>
                                    <option value="11">11 - November</option>
                                    <option value="12">12 - December</option>
                        
                                </select>
                                &nbsp;
                                <select id="inputTypePhone" class="form-control col-sm-4">
                                <option selected>Expire Year *</option>
                              <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
           
                                </select>
                                </div>
                                <br>
                                <div class="form-group row">
                                <input class="form-control col-sm-8" type="text" placeholder="Billing Street Address *">
                                </div>
                                <br>
                                <div class="form-group row">
                                <input class="form-control col-sm-8" type="text" placeholder="Street Address 2 (Apt or Suite Number)">
                                </div>
                                <br>
                                <div class="form-group row">
                                <input class="form-control col-sm-8" type="text" placeholder="Billing City">
                                </div>
                                <br>
                                <div class="form-group row">
                                <select id="inputTypePhone" class="form-control col-sm-4">
                                <option selected>Billing State</option>
                                <option value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="CA">California</option>
                                <option value="CO">Colorado</option>
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="DC">District Of Columbia</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="HI">Hawaii</option>
                                <option value="ID">Idaho</option>
                                <option value="IL">Illinois</option>
                                <option value="IN">Indiana</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NV">Nevada</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NM">New Mexico</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="ND">North Dakota</option>
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="OR">Oregon</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                                </select>
                                &nbsp;
                                <input class="form-control col-sm-4" type="text" placeholder="Billing Zip Code *">
                       
                                </div>
                            <br>
                                
                            <button type="submit" class="btn btn-primary">SUBMIT</button>
                                </form>
                        </div> <!-- end of container -->
                    </div> <!-- end of maContent -->
                </div> <!-- end of maContainer -->
            </div> <!-- end of maPage -->
        </div> <!-- end of col-md-9 -->
        
    </div> <!-- end of row -->
</div> <!-- end of container -->
<?php include( '../../footer.php' ); ?>
</body>
</html>