<?php include( '../../head.php' ); ?>

<!-- START OF STYLES FOR HEADER IN MY ACCOUNT ONLY! -->
<style>
.global-header {
    background:#666;
    position:relative;
}
@media (max-width: 1199px) {
    .global-header {
    background:#fff;
    border-bottom:1px solid #eee;
}
}
li.nav-item.dropdown:last-child > a { 
    color:#ff9a33;
}
</style>
<!-- END OF STYLES FOR HEADER IN MY ACCOUNT ONLY! -->


<div class="container myAccount">
<div class="row">
    <div class="col-md-3">
    <ul id="myAccountMenu">
    <li><a href="/my-account/">My Account</a></li>
    <li><a href="/my-account/personal-information">Personal Information</a></li>
    <li><a href="/my-account/transaction-history">Transcation History</a></li>
    <li class="maActive"><a href="/my-account/billing-information">Billing Information</a></li>
    <li><a href="/my-account/my-sponsorships">My Sponsorships</a></li>
    <li><a href="/my-account/my-monthly-gifts">My Monthly Gifts</a></li>
    </ul>
    </div>

    <div class="col-md-9">
            <div id="maPage">
                <div class="userBar d-flex justify-content-between">
                    <div class="userName align-self-center">Hi, Anthony Matiya</div>
                    <div class="logOut align-self-center"><a href="/">Log Out</a></div>
                </div> <!-- end of userBar -->

                <div id="billingInformation" class="maContainer">
                    <div class="maHeader">
                    <h2> Billing Information </h2>
                    <div class="d-flex justify-content-between">
                    <h3>Payment Methods</h3>
                    <a href="/my-account/billing-information/add-payment"><strong>Add Payment Method</strong></a>
                    </div>
                        <hr>
                    </div> <!-- end of maHeader -->
                    <div class="maContent">

                        <div class="container">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="Radios" id="exampleRadios1" value="option1" checked>
                                                <p class="labelText"><strong>Bank of America</strong></p>
                                        </label>
                                    </div>
                                    <div class="otherLabels">
                                            <p><strong>Payment Type:</strong> <span>Visa</span></p>
                                            <p><strong>Card Number:</strong> <span>***********1984</span></p>
                                            <p><strong>Expiration Date:</strong> <span>11/2021</span></p>
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <p><strong>Billing Address:</strong></p>
                                    <p>Anthony Matiya</p>
                                    <p>715 J street Suite 301</p>
                                    <p>San Diego, CA 92101</p>
                                    <p>USA</p>
                                </div>
                                
                                <div class="col-md-2">
                                    <a href="/my-account/billing-information/edit-payment" class="biBtn">EDIT</a>

                                    <a href="#" class="biBtn">DELETE</a>
                                </div>
                            </div> <!-- end of row for BoA -->

<hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="Radios" id="exampleRadios1" value="option2" >
                                                <p class="labelText"><strong>American Express</strong></p>
                                        </label>
                                    </div>
                                    <div class="otherLabels">
                                            <p><strong>Payment Type:</strong> <span>American Express</span></p>
                                            <p><strong>Card Number:</strong> <span>***********1114</span></p>
                                            <p><strong>Expiration Date:</strong> <span>12/2031</span></p>
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <p><strong>Billing Address:</strong></p>
                                    <p>Anthony Matiya</p>
                                    <p>715 J street Suite 301</p>
                                    <p>San Diego, CA 92101</p>
                                    <p>USA</p>
                                </div>
                                
                                <div class="col-md-2">
                                    <a href="/my-account/billing-information/edit-payment" class="biBtn">EDIT</a>

                                    <a href="#" class="biBtn">DELETE</a>
                                </div>
                            </div> <!-- end of row for AmEx -->
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="Radios" id="exampleRadios1" value="option3">
                                                <p class="labelText"><strong>Wells Fargo Bank</strong></p>
                                        </label>
                                    </div>
                                    <div class="otherLabels">
                                            <p><strong>Payment Type:</strong> <span>Master Card</span></p>
                                            <p><strong>Card Number:</strong> <span>***********1287</span></p>
                                            <p><strong>Expiration Date:</strong> <span>11/2024</span></p>
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <p><strong>Billing Address:</strong></p>
                                    <p>Anthony Matiya</p>
                                    <p>715 J street Suite 301</p>
                                    <p>San Diego, CA 92101</p>
                                    <p>USA</p>
                                </div>
                                
                                <div class="col-md-2">
                                    <a href="/my-account/billing-information/edit-payment" class="biBtn">EDIT</a>

                                    <a href="#" class="biBtn">DELETE</a>
                                </div>
                            </div> <!-- end of row for WFB -->

                        </div> <!-- end of container -->
                    </div> <!-- end of maContent -->
                </div> <!-- end of maContainer -->
            </div> <!-- end of maPage -->
        </div> <!-- end of col-md-9 -->
        
    </div> <!-- end of row -->
</div> <!-- end of container -->
<?php include( '../footer.php' ); ?>
</body>
</html>