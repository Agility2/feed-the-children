<!DOCTYPE html>
<html lang="en">
<head>

	<title>Feed The Children Styles</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Feed The Children">
	<meta name="twitter:card" content="summary">
	<meta name="twitter:description" content="Feed The Children">
	<meta name="twitter:title" content="Feed The Children">
	<meta property="og:locale" content="en_US">
	<meta property="og:type" content="website">
	<meta property="og:title" content="Feed The Children">
	<meta property="og:description" content="Feed The Children">
	<meta property="og:site_name" content="Feed The Children">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
	      integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<!-- FontAwesome CSS -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<!--Google Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
    <link href="styles.css" rel="stylesheet">
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
</head>
<body>
    <div class="menu-ico">
      <span></span>      
      <ul class="menu">
        <li class="home">Home Page Elements</li>
        <li class="interior">Interior Page Elements</li>
        <li class="typography">Typography</li>
        <li class="btn">Buttons & Links</li>
        <li class="icons">Icons</li>
        <li class="grid">Grid System</li>
        <li class="navigationpage">Navigation</li>
        <li class="hero">Hero Blocks</li>
        <li class="logo">Logo usage</l>
        <li class="icons">Icons</li>
        <li class="brand">Brand Colors</li>
        <li class="imagery">Imagery</li>
        <li class="form" >Form Elements</li>
        <li class="dialogs" >Dialogs</li>
      </ul>     
    </div>

        <div class="image">
            <img class="img" src="../images/styles/HOME-ELEMENTS.jpg">
        </div>

    <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
    <script>
        $('.home').click(function(){
                $('.img').attr('src', '../images/styles/HOME-ELEMENTS.jpg');
                $(window).scrollTop(0);
        });
      $('.interior').click(function(){
                $('.img').attr('src', '../images/styles/INTERIOR-ELEMENTS.jpg');
                $(window).scrollTop(0);
        });
        $('.typography').click(function(){
                $('.img').attr('src', '../images/styles/TYPOGRAPHY.jpg');
                $(window).scrollTop(0);
        });
        $('.btn').click(function(){
                $('.img').attr('src', '../images/styles/BUTTONS-LINKS.jpg');
                $(window).scrollTop(0);
        });
        $('.icons').click(function(){
                $('.img').attr('src', '../images/styles/ICONS.jpg');
                $(window).scrollTop(0);
        });
        $('.grid').click(function(){
                $('.img').attr('src', '../images/styles/GRID-SYSTEM.jpg');
                $(window).scrollTop(0);
        });
        $('.navigationpage').click(function(){
                $('.img').attr('src', '../images/styles/NAVIGATION.jpg');
                $(window).scrollTop(0);
        });
        $('.hero').click(function(){
                $('.img').attr('src', '../images/styles/HERO-BLOCKS.jpg');
                $(window).scrollTop(0);
        });
        $('.logo').click(function(){
                $('.img').attr('src', '../images/styles/LOGO-USAGE.jpg');
                $(window).scrollTop(0);
        });
        $('.icons').click(function(){
                $('.img').attr('src', '../images/styles/ICONS.jpg');
                $(window).scrollTop(0);
        });
        $('.brand').click(function(){
                $('.img').attr('src', '../images/styles/BRAND-COLORS.jpg');
                $(window).scrollTop(0);
        });
        $('.imagery').click(function(){
                $('.img').attr('src', '../images/styles/IMAGERY.jpg');
                $(window).scrollTop(0);
        });
        $('.form').click(function(){
                $('.img').attr('src', '../images/styles/FORM-ELEMENTS.jpg');
                $(window).scrollTop(0);
        });
        $('.dialogs').click(function(){
                $('.img').attr('src', '../images/styles/DIALOGS.jpg');
                $(window).scrollTop(0);
        });
        </script>
</body>
</html>