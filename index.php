<?php include( 'head.php' ); ?>

<div class="home"> <!--main home container-->

	<div class="top-background videoBG heroContainer">
		<video autoplay loop>
			<!--			<source src="images/home-video.webm" type="video/webm"/>
			-->
			<source src="images/home-video.mp4" type="video/mp4"/>
		</video>

		<div class="top-background-copy">
			<h2>Hunger knows no borders</h2>
			<p>We FEED families at home, across the U.S. and around the world</p>
		</div>
		<nav class="nav nav-tabs" id="myTab" role="tablist">
			<a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-help" role="tab"
			   aria-controls="nav-help">Help Now</a>
			<a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-see" role="tab"
			   aria-controls="nav-see" aria-expanded="true">See How</a>
		</nav>
	</div>

	<section class="help-now"> <!--begin help now-->
		<div class="container">
			<div class="tab-content" id="nav-tabContent">
				<div class="tab-pane fade show" id="nav-see" role="tabpanel" aria-labelledby="nav-see-tab">
					<div class="row align-items-start justify-content-center">
						<div class="col-12 col-md-6">
							<div class="youtube" data-embed="D_UHMwVPnn4"
							     data-cover="/images/help-now/feed-the-children-video-preview-1.jpg">
								<div class="play-button"></div>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<h2>See how FTC is helping right now</h2>
							<p>This is some placeholder body copy. This is just placeholder text. It will be replaced
								later.</p>
						</div>
					</div>
				</div>
				<div class="tab-pane fade show active" id="nav-help" role="tabpanel" aria-labelledby="nav-help-tab">
					<div class="row">
						<div class="help-left col-md-6 d-flex align-items-center justify-content-start flex-column">
							<form method="post" action="/donate"
							      class="d-flex align-items-center justify-content-center flex-column">
								<div class="form-group"> <!-- begin donation -->
									<h2 class="text-center">Make a Donation</h2>
									<fieldset class="form-group">
										<div class="help-form-item">
											<input type="text" class="form-control" id="donation-25"
											       placeholder="$25.00" readonly>

											<input type="text" class="form-control" id="donation-50"
											       placeholder="$50.00" readonly>
										</div>
										<div class="help-form-item">

											<input type="text" class="form-control" id="donation-100"
											       placeholder="$100.00" readonly>

											<input type="text" class="form-control" id="donation-150"
											       placeholder="$250.00" readonly>
										</div>

										<div class="help-form-item">

											<input type="text" class="form-control" id="donation-500"
											       placeholder="$500.00" readonly>

											<input class="form-control other-amt" type="number"
											       placeholder="Other Amount">
										</div>
										<p class="text-center pt-0 minimum-msg"><span>$10 minimum</span></p>

									</fieldset>
									<div class="form-check" style="margin-left: 6px;">
							<label class="form-check-label">
								<input class="form-check-input dedicateSomeone" type="checkbox" value="">
								Dedicate this donationto someone special
								<a href="#" class="helpLink" data-toggle="tooltip" data-placement="top" title=""
								data-original-title="placeholder text for donation">[?]</a>
							</label>
						</div>
								</div> <!-- end donation -->



								<fieldset class="form-group hidden" id="help-now-dedicate-fields">
									<div class="form-group row">
										<select class="col-sm-12 col-md-6 bg-white">
											<option>In Honor Of</option>
											<option>In Memory Of</option>
										</select>
										<div class="col-sm-12 col-md-6">
											<input class="form-control" type="text" placeholder="Enter Name">
										</div>
									</div>
								</fieldset>

					<script>
	$(".dedicateSomeone").change(function() {
        $("#help-now-dedicate-fields").toggle();
});
</script>
								<button type="submit" class="btn help-now-btn-submit btn-featured">
									Proceed
								</button>
							</form>
						</div>
						<div class="help-right col-12 col-md-6 d-flex flex-column">
							<h2 class="pt-md-0 pt-4 text-center">Help Other Ways</h2>
							<div class="help-item mb-4 mt-3 d-flex align-items-center">
								<div class="help-now-item-icon">
									<img src="images/help-now/Volunteering-Icon-02.png">
								</div>
								<div class="help-item-copy pl-3">
									<p class="font-weight-bold mb-0"><a href="/get-involved/volunteer">Volunteering</a>
									</p>
									<p>We’re looking for volunteers who share our vision — to create a world where no
										child goes to bed hungry.</p>
								</div>
							</div>
							<div class="help-item mb-4 d-flex align-items-center">
								<div class="help-now-item-icon">
									<img src="/images/help-now/Fundraising-Icon-01.png">
								</div>
								<div class="help-item-copy pl-3">
									<p class="font-weight-bold mb-0"><a href="/fundraising-toolkit/">Fundraising</a></p>
									<p>Start your own DIY fundraising campaign and give families the resources and food
										they need to flourish.</p>
								</div>
							</div>
							<div class="help-item mb-4 d-flex align-items-center">
								<div class="help-now-item-icon">
									<img src="images/help-now/Social-Sharing-Icon-03.png">
								</div>
								<div class="help-item-copy pl-3">
									<p class="font-weight-bold mb-0"><a href="/get-involved/social-sharing/">Social
											Sharing</a></p>
									<p>
										We love when you follow us on social media and we love it even more if you share
										and spread the word.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> <!--end help now-->


	<section class="action-map-section"> <!--begin map-->

		<div class="container-fluid"> <!--begin container-->

			<div class="row"> <!--begin row-->


				<!-- =================== LEFT =================== -->

				<div
					class="action-map-left-container col-12 col-sm-12 col-md-12 col-lg-5 col-xl-4 pl-lg-4 pl-xl-5 pr-lg-0">

					<div class="action-map-left pl-lg-4 pl-xl-5">

						<h2>FEED in Action</h2>


						<p class="pr-lg-5">Hunger is a global problem and Feed the Children operates in areas where we
							can make the biggest impact. Use our interactive map to learn more about our work.</p>


						<div class="action-map-areas">

							<label class="btn action-map-button active" data-id="north_america">
								In the USA
								<input value="" name="action-map-area">
							</label>

							<label class="btn action-map-button" data-id="central_america">
								Central America &amp; Caribbean
								<input value="" name="action-map-area">
							</label>

							<label class="btn action-map-button" data-id="south_america">
								South America
								<input value="" name="action-map-area">
							</label>

							<label class="btn action-map-button" data-id="europe">
								Europe
								<input value="" name="action-map-area">
							</label>

							<label class="btn action-map-button" data-id="asia">
								Asia
								<input value="" name="action-map-area">
							</label>

							<label class="btn action-map-button" data-id="africa">
								Africa
								<input value="" name="action-map-area">
							</label>

						</div>


					</div>

				</div>

				<!-- =================== MIDDLE =================== -->

				<div class="action-map-middle-container col-12 col-sm-12 col-md-12 col-lg-7 col-xl-8 p-0">

					<div class="action-map-middle">
						<div class="action-map-middle-map">

							<div class="action-map-google active" id="north_america_id"></div>
							<div class="action-map-google" id="central_america_id"></div>
							<div class="action-map-google" id="south_america_id"></div>
							<div class="action-map-google" id="europe_id"></div>
							<div class="action-map-google" id="asia_id"></div>
							<div class="action-map-google" id="africa_id"></div>

						</div>

					</div>

				</div>

			</div> <!--end row-->

		</div> <!--end container-->

	</section> <!--end action map-->


	<section class="hunger-numbers">

		<div class="hunger-numbers">


			<div class="TWLA-services-section-2">
				<div class="container TWLA-full-width">

					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="hunger-main-title">
								<h2 class="text-center">How We Fight Hunger</h2>
							</div>

						</div>
					</div>

					<div class="row TWLA-row">
						<div class="TWLA-services col-lg-12 col-md-12 col-sm-12 col-12">

							<div class="TWLA-unit col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
								<div class="TWLA-cube">
									<div class="TWLA-unit-wrap-default">
										<div class="hunger-image"><img src="images/hunger-numbers/food-icon.png">
										</div>
										<div class="hunger-caption">
											<div class="hunger-head">Food</div>
											<div class="hunger-text">The key ingredient for a world where no child goes
												to be hungry.
											</div>
											<span class="TWLA-gradient"></span>
										</div>
									</div>
									<div class="TWLA-unit-wrap-hover">
										<div class="hunger-side">
											<h5 class="font-weight-bold">Food</h5>
											<p>Children need nutritious food that helps them grow healthy and strong and
												the younger they start eating healthy, the better.</p>
											<a class="btn btn-primary" href="/our-work/hunger-facts/">How We Help</a>
										</div>
										<span class="TWLA-gradient"></span>
									</div>
								</div>
							</div>

							<div class="TWLA-unit col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
								<div class="TWLA-cube">
									<div class="TWLA-unit-wrap-default">
										<div class="hunger-image"><img src="images/hunger-numbers/water-icon.png">
										</div>
										<div class="hunger-caption">
											<div class="hunger-head">Water</div>
											<div class="hunger-text">We provide access to clean water for drinking,
												bathing and more.
											</div>
											<span class="TWLA-gradient"></span>
										</div>
									</div>
									<div class="TWLA-unit-wrap-hover">
										<div class="hunger-side">
											<h5 class="font-weight-bold">Water</h5>
											<p>There’s a clear link between water and hunger. Dirty water makes children
												sick, and sick children can’t retain the nutrients in food.</p>
											<a class="btn btn-primary" href="/our-work/hunger-facts/">Learn More</a>
										</div>
										<span class="TWLA-gradient"></span>
									</div>
								</div>
							</div>

							<div class="TWLA-unit col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
								<div class="TWLA-cube">
									<div class="TWLA-unit-wrap-default">
										<div class="hunger-image"><img src="images/hunger-numbers/poverty-icon.png">
										</div>
										<div class="hunger-caption">
											<div class="hunger-head">Poverty</div>
											<div class="hunger-text">The root cause of hunger all around the world is
												poverty.
											</div>
											<span class="TWLA-gradient"></span>
										</div>
									</div>
									<div class="TWLA-unit-wrap-hover">
										<div class="hunger-side">
											<h5 class="font-weight-bold">Poverty</h5>
											<p>Of course poverty leads to hunger — but hunger also keeps people in
												poverty because it makes them too sick and too weak to work.</p>
											<a class="btn btn-primary" href="/our-work/hunger-facts/">Attack the
												Root</a>
										</div>
										<span class="TWLA-gradient"></span>
									</div>
								</div>
							</div>

							<div class="TWLA-unit col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
								<div class="TWLA-cube">
									<div class="TWLA-unit-wrap-default">
										<div class="hunger-image"><img src="images/hunger-numbers/health-icon.png">
										</div>
										<div class="hunger-caption">
											<div class="hunger-head">Healthcare</div>
											<div class="hunger-text"><p>Without healthy food, children who get sick stay
													sick longer.</p></div>
											<span class="TWLA-gradient"></span>
										</div>
									</div>
									<div class="TWLA-unit-wrap-hover">
										<div class="hunger-side">
											<h5 class="font-weight-bold">Healthcare</h5>
											<p> Share our resources and give communities access to what they need to
												keep their children healthy.</p>
											<a class="btn btn-primary" href="/our-work/hunger-facts/">Sharing
												Resources</a>
										</div>
										<span class="TWLA-gradient"></span>
									</div>
								</div>
							</div>

							<div class="TWLA-unit col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
								<div class="TWLA-cube">
									<div class="TWLA-unit-wrap-default">
										<div class="hunger-image"><img
												src="images/hunger-numbers/education-icon.png">
										</div>
										<div class="hunger-caption">
											<div class="hunger-head">Education</div>
											<div class="hunger-text"><p>For many, the need to survive outweighs the need
													for education.</p></div>
											<span class="TWLA-gradient"></span>
										</div>
									</div>
									<div class="TWLA-unit-wrap-hover">
										<div class="hunger-side">
											<h5 class="font-weight-bold">Education</h5>
											<p>Going to school is a hallmark of childhood where kids make friends, play
												and begin to explore the world.</p>
											<a class="btn btn-primary" href="/our-work/hunger-facts/">Steps to
												Success</a>
										</div>
										<span class="TWLA-gradient"></span>
									</div>
								</div>
							</div>

							<div class="TWLA-unit col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
								<div class="TWLA-cube">
									<div class="TWLA-unit-wrap-default">
										<div class="hunger-image"><img src="images/hunger-numbers/why-icon.png">
										</div>
										<div class="hunger-caption">
											<div class="hunger-head">Why are Children Hungry?</div>
											<div class="hunger-text"><p>Find out more about the root causes for hunger
													here.</p></div>
											<span class="TWLA-gradient"></span>
										</div>
									</div>
									<div class="TWLA-unit-wrap-hover">
										<div class="hunger-side">
											<h5 class="font-weight-bold">Why are Children Hungry?</h5>
											<p>Learn more about the issues and help us to create a world where no child
												goes to bed hungry.</p>
											<a class="btn btn-primary" href="/our-work/hunger-facts/">Learn More</a>
										</div>
										<span class="TWLA-gradient"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sponsor-child pt-5 pb-5 pt-lg-0 pb-lg-0"> <!--begin sponsor child-->
		<div class="row">
			<div class="shrink-slide col-12 col-lg-10 m-auto">
				<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active"> <!--begin carousel item-->
							<div class="container-fluid">
								<div class="row justify-content-center align-items-center">
									<!--sponsor child image-->
									<div class="sponsor-child-left-container col-12 col-md-5 col-lg-4 col-xl-3">
										<div class="sponsor-left-inner text-center">
											<img class="img-fluid pt-2 pr-2 pl-2" src="images/sponsor/Anthony.jpg">
											<p class="pt-2 pb-2 font-weight-bold">Waiting 200 days</p>
										</div>

										<div class="carousel-control-prev">
											<a href="#carouselExampleControls" role="button"
											   data-slide="prev"><span><</span></a>
											<span class="sr-only">Previous</span>
										</div>
										<div class="carousel-control-next">
											<a href="#carouselExampleControls" role="button"
											   data-slide="next"><span>></span></a>
											<span class="sr-only">Previous</span> <span class="sr-only">Next</span>
										</div>
									</div><!--end sponsor image-->

									<div class="col-12 col-md-4 col-lg-4 col-xl-4 ml-md-5"> <!--sponsor child info-->
										<div class="sponsor-child-middle">
											<h2>Meet Anthony</h2>
											<ul class="d-flex flex-wrap">
												<li class="font-weight-bold"><img
														src="images/sponsor/child-stats-boy.png"
														class="pr-2"><span class="pr-3">Boy</span>
												</li>
												<li class="font-weight-bold"><img
														src="images/sponsor/child-stats-country.png"
														class="pr-2"><span
														class="pr-3">Phillipines</span></li>
												<li class="font-weight-bold"><img
														src="images/sponsor/child-stats-bday.png"
														class="pr-2"><span class="pr-3">Age 13</span>
												</li>
												<li class="font-weight-bold"><img
														src="images/sponsor/child-stats-id.png"
														class="pr-2"><span class="pr-3">158645</span>
												</li>
											</ul>
											<p>Anthony is friendly and in good health. He is living with his parents and
												siblings in a
												community in which the water source is a community water tap. For fun,
												he loves
												to
												participate in games!</p>
											<div
												class="sponsor-buttons d-flex flex-wrap align-items-center flex-column flex-xl-row align-items-baseline">
												<a href="#" class="btn btn-primary">Sponsor Anthony</a>
												<a class="show-form d-none d-lg-block">Find Another Child</a>

												<a class="sponsor-modal-trigger d-lg-none pt-2" data-toggle="modal" data-target="#exampleModal">
													Find Another Child
												</a>
											</div>
										</div>
									</div> <!--end sponsor child info-->
								</div> <!--end row-->
							</div> <!--end container-->
						</div> <!--end carousel item-->

						<div class="carousel-item"> <!--begin carousel item-->
							<div class="container-fluid">
								<div class="row justify-content-center align-items-center">
									<!--sponsor child image-->
									<div class="sponsor-child-left-container col-12 col-md-5 col-lg-4 col-xl-3">
										<div class="sponsor-left-inner text-center">
											<img class="img-fluid pt-2 pr-2 pl-2" src="images/sponsor/Ana.jpg">
											<p class="pt-2 pb-2 font-weight-bold">Waiting 100 days</p>
										</div>

										<div class="carousel-control-prev">
											<a href="#carouselExampleControls" role="button"
											   data-slide="prev"><span><</span></a>
											<span class="sr-only">Previous</span>
										</div>
										<div class="carousel-control-next">
											<a href="#carouselExampleControls" role="button"
											   data-slide="next"><span>></span></a>
											<span class="sr-only">Previous</span> <span class="sr-only">Next</span>
										</div>

									</div><!--end sponsor image-->

									<div class="col-12 col-md-5 col-lg-4 col-xl-4 ml-md-5"> <!--sponsor child info-->
										<div class="sponsor-child-middle">
											<h2>Meet Ana</h2>
											<ul class="d-flex flex-wrap">
												<li class="font-weight-bold"><img
														src="images/sponsor/child-stats-girl.png"
														class="pr-2"><span class="pr-3">Girl</span>
												</li>
												<li class="font-weight-bold"><img
														src="images/sponsor/child-stats-country.png"
														class="pr-2"><span class="pr-3">US</span></li>
												<li class="font-weight-bold"><img
														src="images/sponsor/child-stats-bday.png"
														class="pr-2"><span class="pr-3">Age 15</span>
												</li>
												<li class="font-weight-bold"><img
														src="images/sponsor/child-stats-id.png"
														class="pr-2"><span class="pr-3">158645</span>
												</li>
											</ul>
											<p>Ana is friendly and in good health. She is living with his parents and
												siblings in a
												community in which the water source is a community water tap. For fun,
												he loves
												to
												participate in games!</p>
											<div
												class="sponsor-buttons d-flex flex-wrap align-items-center flex-column flex-xl-row align-items-baseline">
												<a href="#" class="btn btn-primary">Sponsor Ana</a>
												<a class="show-form d-none d-lg-block">Find Another Child</a>

												<a class="sponsor-modal-trigger d-lg-none pt-2" data-toggle="modal" data-target="#exampleModal">
													Find Another Child
												</a>
											</div>
										</div>
									</div> <!--end sponsor child info-->
								</div> <!--end row-->
							</div> <!--end container-->
						</div> <!--end carousel item-->

					</div> <!--end carousel inner-->

				</div> <!--end main carousel container-->

			</div>

			<div class="search-pop modal fade d-lg-none" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><!--mobile search for child modal-->
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Search for a Child</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form>
								<div class="form-group"> <!-- begin billing -->
									<div class="cc-info">
										<select class="form-control boy-girl" required>
											<option value="" selected disabled>Boy or Girl</option>

											<option>Girl</option>
											<option>Boy</option>

										</select>

										<select class="form-control age" required>
											<option value="" selected disabled>Any Age</option>
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
										</select>
									</div>
									<select class="form-control" required>
										<option value="" selected disabled>Any Country</option>
										<option>US</option>
										<option>Honduras</option>
										<option>Nicaragua</option>
									</select>

									<input type="text" class="form-control" placeholder="Birthday"
									       required>
									<select class="form-control" required>
										<option value="" selected disabled>Waiting Longest</option>
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
									</select>

								</div> <!-- end billing -->

								<button type="submit" class="btn btn-primary">Search</button>

							</form>
						</div>

					</div>
				</div>
			</div> <!--end mobile sponsor modal-->

			<div class="col-lg-4 form-slide"> <!--begin form-->
				<div class="inner">
					<h2>Search for a Child</h2>
					<p><a href="#">Looking for</a></p>

					<form>
						<div class="form-group"> <!-- begin billing -->
							<div class="cc-info">
								<select class="form-control boy-girl" required>
									<option value="" selected disabled>Boy or Girl</option>

									<option>Girl</option>
									<option>Boy</option>

								</select>

								<select class="form-control age" required>
									<option value="" selected disabled>Any Age</option>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
								</select>
							</div>
							<select class="form-control" required>
								<option value="" selected disabled>Any Country</option>
								<option>US</option>
								<option>Honduras</option>
								<option>Nicaragua</option>
							</select>

							<input type="text" class="form-control" placeholder="Birthday"
							       required>
							<select class="form-control" required>
								<option value="" selected disabled>Waiting Longest</option>
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>

						</div> <!-- end billing -->

						<button type="submit" class="btn btn-primary">Search</button>

					</form>

				</div>

			</div> <!--end form-->

		</div>


	</section> <!--end sponsor chuld-->

	<section class="sponsor-truck mb-5 mt-5">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-md-6 mb-5 mb-md-0">
					<h2>Feed a Community</h2>
					<p>Lorem ipsum dolor site emet, consectetuer adipicing elit. Maruoejs rius forn srjeja. Lorem ipsum
						dolor site emet, consectetuer adipicing elit. Maruoejs rius forn srjeja. Lorem ipsum dolor site
						emet, consectetuer adipicing elit. Maruoejs rius forn srjeja.</p>
					<a class="btn btn-primary" href="/get-involved/sponsor-a-truck/ ">Sponsor a Truck</a>
				</div>
				<div class="col-12 col-md-6">
					<img class="img-fluid" src="images/sponsor/feed-truck.png">
				</div>
			</div>
		</div>
	</section>

</div> <!--end home-->

<?php include( 'footer.php' ); ?>
